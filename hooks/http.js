import { useState, useEffect } from 'react';
import AsyncStorage from '@react-native-community/async-storage';

export const useHttp = (body, dependencies, type, id) => {
  const [isLoading, setIsLoading] = useState(true);
  const [fetchedData, setFetchedData] = useState(null);


  //   fetch('https://swapi.co/api/people')
  useEffect(() => {
    setIsLoading(true);
    //onsole.log('Sending Http request to URL: ' + body);

    // AsyncStorage.removeItem('allEntries').then()
    //getData(body, setIsLoading, setFetchedData,type);
    if (type == "allEntries" && !dependencies[1]) {
   
    
      // AsyncStorage.removeItem('allEntries_time').then()
      AsyncStorage.getItem('allEntries_time').then(
        result => {

         
          if (result) {
           
            let valid = isValidData(result,1);

            if (valid) {

              AsyncStorage.getItem(type).then(
                result => {
                  setIsLoading(false);
                  setFetchedData(JSON.parse(result));
                }
              );
              return;
            }
            else{
              getData(body, setIsLoading, setFetchedData,type);
            }

          }
          else{
            getData(body, setIsLoading, setFetchedData,type);
          }

        }
      )

    }


    else if (type == "article") {
      // AsyncStorage.removeItem('article_time').then()
      
      let key1 = 'article_time'+id;
      AsyncStorage.getItem(key1).then(
        result => {
          if (result) {
         
            let valid = isValidData(result, 24);
            
            if ( valid) {
              let key = type+id;
              AsyncStorage.getItem(key).then(
                result => {
                  setIsLoading(false);
                  setFetchedData(JSON.parse(result));
                }
              );
              return;
            }
            else{
              getData(body, setIsLoading, setFetchedData,type, id);
            }
           
          } else{
             getData(body, setIsLoading, setFetchedData, type, id);
          }
        }
      )
    }


    else if (type == "allEmergencyNumbers") {
      AsyncStorage.getItem('allEmergencyNumbers_time').then(
        result => {

        
          if (result) {
            let valid = isValidData(result,4);
            if (valid) {

              AsyncStorage.getItem(type).then(
                result => {
                 
                  setFetchedData(JSON.parse(result));
                  setIsLoading(false);
                }
              );
              return;
            }
            else{
             
              getData(body, setIsLoading, setFetchedData,type, id);
            }
          }
          else{
            getData(body, setIsLoading, setFetchedData,type, id);
          }

        }
      )

    }

    else{
      getData(body, setIsLoading, setFetchedData,type)
    }










  }, dependencies);

  return [isLoading, fetchedData];
};

const dateFormat = value => {
  if (value < 10)
    return `0${value}`;
  return value;
}

const isValidData = (storageTime, time) => {


  try {
    let _time = (time)?time:2;
    let date1 = new Date(parseInt(storageTime, 10));
    let date2 = new Date();

    return (date2.getTime() - date1.getTime()) < (_time * 60 * 60 * 1000);
  }
  catch (error) {
    return false;
  }

}



const getData = (body, setIsLoading, setFetchedData,type, id) => {


  fetch("https://covid19-caboverde.herokuapp.com/admin/api", {
    "method": "POST",
    "headers": {
      "content-type": "application/json",
      "accept-encoding": "gzip;q=1.0, compress;q=0.5"
    },
    "body": body
  })
    .then(response =>{
        //console.log(response)
        return response.text()
    })  // promise
    .then(async text => {
      const result = JSON.parse(text);
      
      if (type == "article"){
        let key = type+id;
        let allArticle = result.data.Article;
        setFetchedData(allArticle);
        setIsLoading(false);
        await AsyncStorage.setItem(key, JSON.stringify(allArticle));
        let dateNow = new Date();
        let key1 = 'article_time'+id;
        await AsyncStorage.setItem(key1, '' + dateNow.getTime());
      }
      else if (type == "allEmergencyNumbers") {
        let allContact = result.data.allEmergencyNumbers;
        setFetchedData(allContact);
        setIsLoading(false);
        await AsyncStorage.setItem(type, JSON.stringify(allContact));
        let dateNow = new Date();
        await AsyncStorage.setItem("allEmergencyNumbers_time", '' + dateNow.getTime());
      }
      else if (type == "allEntries") {

        let dateNow = new Date();
        let allEntries = result.data.allEntries.map(entry => {
            entry.date = dateNow;
            return entry;
        });
        setFetchedData(allEntries);
        setIsLoading(false);
        await AsyncStorage.setItem(type, JSON.stringify(allEntries));
        await AsyncStorage.setItem("allEntries_time", '' + dateNow.getTime());
      }
      else {
        setFetchedData(result.data.allEntries);
        setIsLoading(false);
      }
    })
    .catch(err => {
 
        //onsole.log(err)

      let key = (id)?type+id:type;

      AsyncStorage.getItem(key).then(
                    result => {
                      if(result){
                        setIsLoading(false);
                        setFetchedData(JSON.parse(result));
                      }
                      else{
                        setIsLoading(false);
                        // setFetchedData([]);
                      }
                     
                    }
                  );

    });
}