
import React, { useState, useEffect } from 'react';
import {Modal, Text, TouchableHighlight, View, Alert} from 'react-native';

// export default class ModalExample extends Component {
export default  Modal =props => { 

    const [modalVisible, setModalVisible] = useState(false);

   let content = (
      <View style={{marginTop: 200, marginLeft:20}}>
        <Modal
          animationType="slide"
          transparent={false}
          visible={modalVisible}
          onRequestClose={() => {
            setModalVisible(false);
          }}>
          <View style={{marginTop: 22}}>
            <View>
              <Text>Hello World!</Text>

              <TouchableHighlight
                onPress={() => {
                  setModalVisible(!modalVisible);
                }}>
                <Text>Hide Modal</Text>
              </TouchableHighlight>
            </View>
          </View>
        </Modal>

        <TouchableHighlight
          onPress={() => {
                setModalVisible(true);
          }}>
          <Text>Show Modal</Text>
        </TouchableHighlight>
      </View>
    );

    return content;
  
}