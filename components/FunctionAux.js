
import { Platform, Linking, Alert, } from 'react-native';
import * as RNLocalize from "react-native-localize";
import I18n from '../I18n';

 const callNumber = phone => {
    //onsole.log('callNumber ----> ', phone);
    let phoneNumber = phone;
    if (Platform.OS !== 'android') {
        phoneNumber = `telprompt:${phone}`;
    }
    else {
        phoneNumber = `tel:${phone}`;
    }
    Linking.canOpenURL(phoneNumber)
        .then(supported => {
            if (!supported) {
                Alert.alert('Phone number is not available');
            } else {
                return Linking.openURL(phoneNumber);
            }
        })
        .catch(err =>  Alert.alert('Phone number is not available'));
};


const openLink = url => {
   
  Linking.canOpenURL(url).then(supported => {
    if (supported) {
      Linking.openURL(url);
    } else {
      alert("Don't know how to open URI: " + url);
    }
  }).catch(err =>  Alert.alert(err));

  
// if (Platform.OS === `ios`) {
//      Linking.canOpenURL(`inbox-gmail:`).then(
//       supported => {
//         // if inbox is installed, open that
//         if (inbox) {
//           Linking.openURL(`inbox-gmail:`);
//         } else {
//         // else default to iOS Mail app
//           Linking.openURL(`message:`);
//         }
//       }
//      ).catch(err =>  Alert.alert(err));

    
// } else if (Platform.OS === `android`) {
//     // open default mail app via exposed method for Android via logic like: http://stackoverflow.com/a/31594815/1681896
//    // Mailer.openDefaultClient(); 
// }

};



const searchInput = (text, _array) => {
    if(!_array || _array.length==0)
        return [];
    text=text.trim();

    if(text=='')
        return _array;

    const newData = _array.filter(item => {
        const itemData = I18n.t(`countries.${item.country.code}`).toUpperCase();
        const textData = text.toUpperCase();
        return itemData.indexOf(textData) > -1;
    });

    return newData;
}


  const isValidData = (dateString, time) => {


    try {
      let _time = (time)?time:2;
      let date1 = new Date(dateString);
      let date2 = new Date();
  
      return (date2.getTime() - date1.getTime()) < (_time * 60 * 60 * 1000);
    }
    catch (error) {
      return false;
    }
  
  }
  
  const isLanguagePT =()=>{
    // alert(JSON.stringify(RNLocalize.getLocales()[0].languageCode))
  return  (RNLocalize.getLocales()[0].languageCode=="pt")
  }

  /*
  format the date to string 
  default - Segunda-Feira, 23 de Março de 2020 20:30
  LLL -  23 de Março de 2020 20:30
  */ 
  const formatDate = (_date, type) => {
   
   const isPT =isLanguagePT();
    try {
      let date = new Date(_date)
      // var options = {  weekday: (isPT)?'short':'long', year: 'numeric', month: 'long', day: 'numeric', hour: '2-digit', minute: '2-digit',  hour12: (isLanguagePT)?false:true }
    
      // if(type=='LLL')
      //     options = {  year: 'numeric', month: 'long', day: 'numeric', hour: '2-digit', minute: '2-digit',  hour12: (isPT) ?false:true }
      
      //     return new Date().toLocaleTimeString((isPT)?'pt-pt':'en-us', options);
    
      let time = date.toLocaleTimeString();
     
      time = time.split(':'); // convert to array

      let hours = time[0];
      let minutes = time[1];
      let dd= date.getDate();
      let yy= date.getFullYear();
      let dayWeek = I18n.t(`days.${date.getDay()}`);
      let months = I18n.t(`months.${date.getMonth()+1}`);
      let hoursEN = `${hours%12}:${minutes} ${(hours/12 >= 1) ? "PM" : "AM"}`;
      let hoursPT = `${hours}:${minutes}`;


         return (!isPT)? `${(type=="LLL")?'':(dayWeek+', ')}${months} ${dd}, ${yy}, ${hoursEN}`:
                    `${(type=="LLL")?'':(dayWeek+', ')}${dd} de ${months} de ${yy}, ${hoursPT}`
    }
    catch (error) {
    }
    return "-";
   
    }
  
export {
    callNumber,
    searchInput,
    openLink, 
    isValidData,
    isLanguagePT,
    formatDate,
  }
  