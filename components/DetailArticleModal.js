
import React from 'react';
import { Modal,  View, Alert } from 'react-native';


export default DetailArticleModal = props => {

  let content = (
    <View style={{ flex: 1 }} >
      <Modal
        style={{ flex: 1 }}
        animationType="slide"
        transparent={false}
        visible={props.modalVisible}
        startInLoadingState={true}
        onRequestClose={() => {
            props.setModalVisible(false);
        }}
      >

        {props.children}

      </Modal>

    </View>
  );

  return content;

}
