
import React from 'react';
import { View, TouchableOpacity, StyleSheet, Text, FlatList } from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';

export default TitleHeader = props => {

    if (props.back) {
        return (
            <View style={styles.headerWrapperBack}>
                <TouchableOpacity
                style={{ paddingRight:10 }}
                    // style={{ position: "absolute", left:0, marginTop:23 }}
                    onPress={() => {
                        
                        if(props.setModalVisible)
                                props.setModalVisible(false)
                        else
                            props.navigation.goBack();
                    }}
                > 
                    <View style={styles.headerBoxBack}>
                        <Icon style={{ paddingTop: 10, marginLeft: 10 }} name="left" size={20} color={"#ffffff"} />
                    </View>

                </TouchableOpacity>
                

                <Text style={styles.headerText} >{props.title}</Text>

                <TouchableOpacity
                     style={{ paddingHorizontal:10 }}
                    onPress={props.onPress}
                > 
                    <View style={styles.headerBoxBack}>
                      { (props.onPress)?  
                      <Icon style={{ paddingTop: 10, marginLeft: 10 }} name="reload1" size={25} color={"#ffffff"} />:
                      <View style={{ paddingHorizontal: 10 }}  />}
                    </View>

                </TouchableOpacity>
            </View>
        )
    }
    return (
        <View style={styles.headerWrapper}>
            <Text style={styles.headerText} >{props.title}</Text>
        </View>
    );
}


const styles = StyleSheet.create({

    headerWrapper: {
        paddingVertical: 30,
        paddingHorizontal: 10,
        flex: 1,
        backgroundColor: "rgba(63, 111, 159, 0.9)",
    },

    headerText: {
        fontSize: 30,
        color: "rgba(255, 255, 255, 0.9)",
        fontFamily: "BebasNeue",
      marginTop:5
        // textAlign:"center",
    },

    headerWrapperBack: {
        paddingVertical: 30,
        paddingHorizontal: 10,
        flex: 1,
        backgroundColor: "rgba(63, 111, 159, 0.9)",
        flexDirection:"row",
        justifyContent: "space-between",
        alignItems:"center"
    }


});


