import React, { Component } from 'react';
import { View,TouchableOpacity, Text, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import moment from "moment";
import Flag from 'react-native-flags';
import NetInfo from "@react-native-community/netinfo";
import Toast from 'react-native-simple-toast';
import I18n from '../I18n';
const ItemList = props  => (
  

  // <FadeInView >
   <TouchableOpacity onPress={()=>{

    if (props.isDetail) {
        return;
    }
    
    NetInfo.fetch().then(state => {
      if(state.isConnected){

         props.navigation.push( 
              'Details',
              {
                countryName: props.data.country.name,
                code: props.data.country.code,
                date: props.data.date,
                isFavorite: props.data.isFavorite,
                setUpdated:props.setUpdated,
              }
              );
      }
      else{
        Toast.showWithGravity(I18n.t(`internetError`), Toast.LONG, Toast.TOP);
      }
    });

     

  }}>
    <View style={styles.itemList}>
      
    <View style={styles.itemListBox}>
      <View >
      <View style={{flexDirection:"row", alignItems:"center" }}>
      {props.data.country && <View style={styles.flagCountryContainer}>
         <Flag code={props.data.country.code} size={24}></Flag>
        <Text style={styles.itemListName}>{I18n.t(`countries.${props.data.country.code}`)}</Text>
         </View>
         } 
                { (props.data.isFavorite) && <Icon  style={{marginLeft:10, marginBottom:5}} name={(props.data.isFavorite)?'ios-star':'ios-star-outline'} size={18} color={"#3f6f9f"} />}
                 </View>
        {props.isDetail && <Text style={styles.itemListName}>{`${dateFormat(props.data.date)}`}</Text>}
       
        <View style={styles.itemListTexts}>
          <Text style={styles.itemListDetail}> {`${I18n.t("simpleText.confirmed")}: ${props.data.totalCases}`}</Text>
          <Text style={styles.itemListDetail}> {`(${(props.data.newCases)?props.data.newCases:0})  | `}</Text>
          <Text style={styles.itemListDetail}> {`${I18n.t("simpleText.deaths")}: ${(props.data.totalDeaths)?props.data.totalDeaths:0}`}</Text>
          <Text style={styles.itemListDetail}> {`${getText(props.data.newDeaths)}  | `}</Text>
          
          <Text style={styles.itemListDetail}> {`${I18n.t("simpleText.recovered")}: ${(props.data.cured)?props.data.cured:0}  | `}</Text>
          <Text style={styles.itemListDetail}> {`${I18n.t("simpleText.critical")}: ${(props.data.critical)?props.data.critical:0}`}</Text>

        </View>
      </View> 

        {  <Icon name="ios-arrow-forward" size={12} color="grey" />}
        </View>
        
        <View style={styles.line}/>


      </View>
      </TouchableOpacity>
  
  // </FadeInView>
  );


  const getText = value =>{
    if(value)
      return `(${value})`;
    return '';
  }

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    // backgroundColor:'#f5f5f5'
  },
  containerIsLoading: {
    flex: 1,
    justifyContent:'center',
    alignItems: 'center',
    backgroundColor:'#f5f5f5'
  },
  flagCountryContainer:{
    flexDirection:"row",
    alignItems:"center",
   
  },
  list: {
    marginTop:20,
    marginBottom:180,
 },

 itemList: {
    borderRadius:4,
     paddingVertical:5,
    //  marginHorizontal:10,
     paddingHorizontal:15,
    //  backgroundColor:"rgba(63, 111, 159, 0.2)",
     // backgroundColor:'#DCDCDC'
 },
 itemListBox:{
   flexDirection:'row',
   justifyContent:'space-between',
   alignItems:"center",
   marginRight:10,
 },

 line:{
   backgroundColor:'#DCDCDC',
   width:'100%',
   height:1,
   marginTop:15,
 },
 itemListName:{
   
   fontSize:20,
   marginBottom:3,
   fontFamily: "BebasNeue",
   marginLeft:6,
   marginTop:3,
 },
 itemListDetail:{
   marginTop:5,
   fontSize:14,
   opacity:0.7,
  fontFamily: "BebasNeue", 
  fontWeight:"100",
  // letterSpacing:1
 },
 itemListTexts:{
   flexDirection:'row',
   flexWrap:"wrap",
   paddingRight:10
 },

});

export default ItemList;

const dateFormat = value =>{
  try{  
    if(value)
      return moment(new Date(value)).format("YYYY-MM-DD");
  }
  catch(error){
  }
  return "-";
}