
import React, { useState, useEffect } from 'react';
import { View, Image, Picker, ActivityIndicator, Text, StyleSheet, SafeAreaView, FlatList, TextInput, TouchableOpacity, Dimensions } from 'react-native'; import MapView, { Marker, Circle, Callout, PROVIDER_GOOGLE } from 'react-native-maps';
import { useHttp } from './../hooks/http';
import CardHeader from '../components/CardHeader';
import ItemList from '../components/ItemList';
import AsyncStorage from '@react-native-community/async-storage';
import I18n from '../I18n';
import Icon from 'react-native-vector-icons/AntDesign';
import NetInfo from "@react-native-community/netinfo";
import Toast from 'react-native-simple-toast';



export default OverviewTabContent = props => {



  let body = `{\"query\":\"query {  allEntries(    where: { latest: true }  ) {    date   country {      code    lat      lng    }    newCases    newDeaths    totalCases cured  critical totalDeaths  }}\"}`;
  const [update, setUpdated] = useState(false);
  const [isLoading, fetchedData] = useHttp(body, [update, props.refreshing], "allEntries");
  

  const [totalData, setTotalData] = useState(null);
  const [listData, setListData] = useState(null);
  const [searchText, onChangeText] = useState('');
  const [sortValue, setSortValue] = useState('totalCases');
  // const [refreshing, setRefreshing] = useState(false);

  const [countryDetail, setCountryDetail] = useState(null);




if(props.refreshing){
  props.setRefreshing(false);
}


  if (fetchedData ) {

    

    if(!listData ||  update){
      
      AsyncStorage.getItem('favoritesCountry').then(
 
       
        result => {
         
          let favotites = [];
          if(result){
            favotites= JSON.parse(result);
          }
          const list=[];

          fetchedData.forEach(item=>{
            if(favotites.filter(code => code ==item.country.code).length==0)
                {
                  item.isFavorite=false;
                  item.country.CountryName=I18n.t(`countries.${item.country.code}`);
                }
            else{
              // alert(favotites)
              item.isFavorite=true;
            }
                

                list.push(item);
          });

          // alert(JSON.stringify(list[0].country.CountryName))
          // alert(b.coutry);
          //list.sort((b, a) => (a.isFavorite > b.isFavorite) ? 1 : -1);
          (sortValue=='name')?list.sort((a, b) => (b.isFavorite > a.isFavorite) ? 1 : (a.isFavorite === b.isFavorite) ? ((a.country.CountryName > b.country.CountryName) ? 1 : -1) : -1 )
          :list.sort((a, b) => (b.isFavorite > a.isFavorite) ? 1 : (a.isFavorite === b.isFavorite) ? ((b.totalCases > a.totalCases) ? 1 : -1) : -1 )

          setListData(list);
        }
      );
 
    }
        
    let total = {
      "newCases": 0,
      "newDeaths": 0,
      "totalCases": 0,
      "totalDeaths": 0,
      "critical": 0,
      "totalCured": 0,
    };

    fetchedData.forEach(element => {
   
      if (element.newCases)
        total.newCases += element.newCases;
      if (element.newDeaths)
        total.newDeaths += element.newDeaths;
      if (element.totalCases)
        total.totalCases += element.totalCases;
      if (element.totalDeaths)
        total.totalDeaths += element.totalDeaths;
      if (element.critical)
        total.critical += element.critical;
      if (element.cured)
        total.totalCured += element.cured;

    });

    if (!totalData)
      setTotalData(total)

    if (update)
    setUpdated(false)
  }


  let content = (<View style={styles.containerIsLoading}><ActivityIndicator size="large" color="#3f6f9f" /></View>);

  if (!isLoading && fetchedData && fetchedData.length > 0) {

    content = (

        <View style={styles.container}>

            <Text style={styles.exploreText}>{I18n.t("simpleText.cardHeaderTitle")}</Text>
            <View style={styles.mainContainer}>  

        
            {totalData && <CardHeader 
                                fetchedData ={fetchedData} 
                                onChangeText={onChangeText} 
                                setSortValue={setSortValue} 
                                sortValue={sortValue} 
                                // listData={listData} 
                                setListData={setListData} 
                                setUpdated={setUpdated} 
                                totalData={totalData} />}


            <View style={styles.list}>

              {/* <View style={styles.textInputContainer}>
        <Icon name="ios-search" size={25} color="grey" />
          <TextInput
          style={styles.textInput}
            onChangeText={text => onChangeText(text)}
            value={value}
            placeholder="Search Location"
          />
        </View> */}

              {/* <Picker selectedValue = {user} onValueChange = {updateUser}>
               <Picker.Item label = "Steve" value = "steve" />
               <Picker.Item label = "Ellen" value = "ellen" />
               <Picker.Item label = "Maria" value = "maria" />
        </Picker> */}




              {listData && <FlatList
              keyboardShouldPersistTaps={'always'}
              style={styles.flatList}
                data={listData}
                renderItem={({ item }) => <ItemList  setUpdated ={setUpdated} data={item} setCountryDetail={setCountryDetail} navigation={props.navigation} />}
                keyExtractor={item => item.country.code}
              />}

          </View>
            </View>


        </View>

    );
  } 
  
    else if (!isLoading && (!fetchedData || fetchedData.length === 0)) {
     content = (<View style={styles.containerIsLoading}>
       <Text>{I18n.t(`textError`)}</Text>
       <TouchableOpacity
         style={{ marginTop:10}}
          onPress={()=>{
            NetInfo.fetch().then(state => {
              if(state.isConnected)
                props.setRefreshing(true);
              else{
                Toast.showWithGravity(I18n.t(`internetError`), Toast.LONG, Toast.TOP);         

              }
              })
            }
           
          }

        
        >
         <View hitSlop={{top: 50, bottom: 50, left: 50, right: 50}} style={{zIndex:3}}>
                  <Icon style={{ padding: 10 }} name="reload1" size={25} color={"grey"} />
                </View>
    </TouchableOpacity>
       </View>
       );
     }
  return content;
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f5f5f5'
  },
  containerIsLoading: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f5f5f5',
    height:200,
  },
  dateContainer: {
    marginTop: 20,
    alignItems: 'center',
  },
  dateContainerText: {
    opacity: 0.7,
    fontSize: 13,
  },

  mainContainer:{
   
    marginHorizontal:10,
borderRadius:16,
    backgroundColor:"rgba(63, 111, 159, 0.2)",
  },
  textInputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: "center",
    //backgroundColor: "#FFFAF0",
    //borderColor: 'gray',
    //borderWidth: 1,
    marginHorizontal: 15,
    //paddingHorizontal: 10,
    borderRadius: 10,
    marginVertical: 15,
  },

  textInput: {
    //height: 40,
    // width:'70%',
    flex: 1,
    // borderColor: 'gray',
    //  borderWidth: 1,
    //marginHorizontal: '1%',
    //paddingHorizontal: 5,
    //  borderRadius:10,
    //marginVertical: 5,

  },

  exploreText: {
    // marginBottom:10,
    marginTop: 10,
    marginBottom: 10,
    marginHorizontal: 10,
    fontSize: 30,
    fontWeight: "bold",
     fontFamily: "BebasNeue",
  
  },

  tabTopBox: {
    width: "23%",
    // padding: 20,
    paddingVertical:15,
    borderRadius: 8,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor:"transparent",
  },

  tabTopBoxSelected: {
    width: "31%",
    // padding: 20,
    paddingVertical:15,
    borderRadius: 8,
    justifyContent: "center",
    alignItems: "center",
   
    backgroundColor:"#194A72",

  },
  tabTopText: {
    // color: "white",
    textTransform:"uppercase",
    fontSize:14,
    marginTop:5,
    fontFamily: "BebasNeue",
  },

  tabTopContainer: {
    flexDirection: "row",
    paddingVertical: 0,
    paddingHorizontal: 10,
    backgroundColor:"rgba(63, 111, 159, 0.2)",
    paddingVertical:25
  },

  titleHeaderImage:{
    justifyContent:"center",
    alignItems:"center",
    marginVertical:8
  },

  flatList:{
      minHeight: Dimensions.get('window').height*0.7,
  }

});


const TabHeader = props => {

  return (

    <TouchableOpacity
      style={[styles.tabTopBox,(props.tabTopValue == props.value)? styles.tabTopBoxSelected:styles.tabTopBox]}
      setTabTopValue
      // onPress={() => { props.setTabTopValue('overview') }}
       onPress={props.onPress}
    >
       <Image style={{ width: 60, height: 60 }} source={props.iconPath} />
        <Text style={[styles.tabTopText,{color: (props.tabTopValue == props.value)?"#fff":"#3f6f9f"}]} >{props.text}</Text>
    </TouchableOpacity>

  )
}


