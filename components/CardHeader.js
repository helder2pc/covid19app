import React, { useState } from 'react';
import { View, TextInput, Text, StyleSheet, TouchableOpacity, Platform } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import moment from "moment";
import I18n from '../I18n';
import { isLanguagePT, searchInput, formatDate } from './FunctionAux';

const CardHeader = props => {
  // const[totalData] = props;
  // const [value, onChangeText] = useState('');

  // alert(JSON.stringify(props.totalData))
  return (
    <View style={[styles.container,
    (props.isDetail) ? styles.cardDetail : ''
    ]
    }>

      <View style={styles.rowContainer}>
        <View style={styles.itemContainer}>
          <Text style={styles.itemContainerText}>{props.totalData.totalCases}</Text>
          <View style={styles.itemBox}>
            <Text style={styles.itemBoxText}>{I18n.t("simpleText.confirmed").toUpperCase()}</Text>
          </View>
          {props.isDetail && <Text style={styles.itemContainerTextNew}>{props.totalData.newCases}</Text>}
        </View>

        <View style={styles.itemContainer}>
          <Text style={styles.itemContainerText}>{props.totalData.totalDeaths}</Text>
          <View style={styles.itemBox}>
            <Text style={styles.itemBoxText}>{I18n.t("simpleText.deaths").toUpperCase()}</Text>
          </View>
          {props.isDetail && <Text style={styles.itemContainerTextNew}>{props.totalData.newDeaths}</Text>}
        </View>

        <View style={styles.itemContainer}>
          <Text style={styles.itemContainerText}>{props.totalData.totalCured}</Text>
          <View style={styles.itemBox}>
            <Text style={styles.itemBoxText}>{I18n.t("simpleText.recovered").toUpperCase()}</Text>
          </View>
          {/* {props.isDetail &&<Text style={styles.itemContainerTextNew}>0</Text>} */}
          {/* {props.isDetail && <Text style={styles.itemContainerTextNew}>{props.totalData.cured}</Text>} */}
        </View>

      </View>

      {(props.fetchedData && props.fetchedData.length > 0 && !props.isDetail) &&
        <Text style={styles.dateText}>{formatDate(new Date(props.fetchedData[0].date))}</Text>}
      {/* <Text style={styles.dateText}>{dateFormatFull(date)}</Text> */}

      <View style={{ flexDirection: "row", alignContent: "center", justifyContent: "center" }}>
        {!props.isDetail && <View style={styles.textInputContainer}>
          <Icon name="search" size={22} color="#AfAfAf" />
          <TextInput
            style={styles.textInput}
            onChangeText={(text) => {
              props.onChangeText(text);
              props.setListData(searchInput(text, props.fetchedData))
            }}
            // value={value}
            placeholder={I18n.t("searchPlaceholder")}
          />

        </View>}

        {!props.isDetail &&
          <TouchableOpacity
            style={{ width: 32, justifyContent: "center", marginRight: 12 }}
            // {/* onPress={props.onPress} */}
            onPress={() => {
              props.setSortValue((props.sortValue == 'totalCases') ? 'name' : 'totalCases');
              props.setUpdated(true);
            }}
          >

            <Icon name={(props.sortValue == 'totalCases') ? 'sort-alpha-asc' : 'sort-numeric-desc'} size={25} color="white" />
          </TouchableOpacity>
        }
      </View>

    </View>
  )

}

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    backgroundColor: '#194A72',
    marginBottom: 10,
    borderRadius: 16

  },
  containerIsLoading: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f5f5f5'
  },

  rowContainer: {
    // flex:1,
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 30

    // alignItems:'center'

  },
  itemContainer: {

    alignItems: 'center',
    marginHorizontal: 15,
  },
  itemContainerText: {
    color: '#fff',
    fontSize: 26,
    fontFamily: "BebasNeue",
  },
  itemContainerTextNew: {
    color: '#3CB371',
    fontFamily: "BebasNeue",
    // color: '#fff',
    fontSize: 20,
  },
  itemBox: {
    backgroundColor: '#3f6f9f',
    padding: 11,
    borderRadius: 10,
    marginTop: 6,
    marginBottom: 6,

  },
  itemBoxText: {
    color: 'white',
    fontSize: 16,
    fontFamily: "BebasNeue",
  },

  textInputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: "center",
    backgroundColor: "#FFFAF0",
    borderColor: 'gray',
    borderWidth: 1,
    marginHorizontal: 15,
    paddingHorizontal: 10,
    paddingVertical: Platform.OS === 'ios' ? 12 : 0,
    borderRadius: 10,
    marginVertical: 15,
    flexGrow: 1
    // width:"100%",
    // width:'85%',
  },

  textInput: {
    color: "black",
    // width:'70%',
    flex: 1,
    // borderColor: 'gray',
    //  borderWidth: 1,
    paddingHorizontal: 5,
    //  borderRadius:10,

  },

  cardDetail: {
    paddingHorizontal: 10,
    paddingBottom: 30,
    marginBottom: 20,
    marginHorizontal: 10,
  },
  dateText: {
    fontSize: 14,
    marginTop: 5,
    marginVertical: 5,
    color: "#fff",
    textAlign: "center"
  },

});

export default CardHeader;


// const dateFormatFull = value => {


//   try {
//     moment.locale('pt');
//     //const lan = isLanguagePT()?'pt':'lan';
//     console.log(moment(new Date(value)).format("LLLL", 'pt'))
//     if (value)
//       return moment(new Date(value)).format("LLLL", 'pt');
//   }
//   catch (error) {
//   }
//   return "-";
// }