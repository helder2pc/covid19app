import React, { Component } from 'react';
import { View, TouchableOpacity, Text, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import ParsedText from 'react-native-parsed-text';
import FadeInView from './FadeInView';
import NetInfo from "@react-native-community/netinfo";
import { formatDate } from './FunctionAux';
import I18n from '../I18n';
const ItemNews = props => (

  <FadeInView delay={props.delay}> 

  <TouchableOpacity onPress={() => {
    // alert(props.)
   
    NetInfo.fetch().then(state => {
      // alert("Connection type", state.type);
      if(state.isConnected){

        props.setModalVisible(true);
        props.setUrl(props.data.url);
        
      }
      else{
        Toast.showWithGravity(I18n.t(`internetError`), Toast.LONG, Toast.TOP)
      }
    });


  }}>
    <View style={styles.itemList}>

      <View style={styles.itemListBox}>
        <View >

          {/* <Text style={styles.itemListName}>{props.data.title}</Text> */}
          <ParsedText
            style={styles.itemListName}
            parse={
              [
                { pattern: /#(\w+)/, style: styles.hashTag },
                { pattern: /@(\w+)/, style: styles.hashTag },
                {type: 'url', style: styles.hide},
              ]
            }
            childrenProps={{ allowFontScaling: false }}
          >
            {removeLink(props.data.title)}
          </ParsedText> 


          <Text style={styles.itemListDetail}>{formatDate(new Date (props.data.date))}</Text>
        </View>

      </View>

      <View style={styles.line} />

    </View>
  </TouchableOpacity>
  </FadeInView>
);


const styles = StyleSheet.create({
  container: {
    // flex: 1,
    // backgroundColor:'#f5f5f5'
  },
  containerIsLoading: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f5f5f5'
  },

  list: {
    marginTop: 20,
    marginBottom: 180,
  },

  itemList: {
    paddingVertical: 5,
    paddingHorizontal: 15,
    // backgroundColor:'#DCDCDC'
  },
  itemListBox: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: "center",
    marginRight: 10,
  },

  line: {
    backgroundColor: '#DCDCDC',
    width: '100%',
    height: 1,
    marginTop: 15,
  },
  itemListName: {
    fontSize: 16,
  },
  itemListDetail: {
    marginTop: 5,
    fontSize: 14,
    opacity: 0.7
  },
  itemListTexts: {
    flexDirection: 'row',
  },

  hashTag: {
    //fontStyle: 'italic',
    color: 'rgb(1,190,229)',
    fontSize: 15,
  },


  hide: {
    color: 'red',
    display:"none",

  },


});

export default ItemNews;



const removeLink = text => {

  var a = text.replace(/(?:https?|ftp):\/\/[\n\S]+/g, '');
  var b = a.replace(/(?:http?|ftp):\/\/[\n\S]+/g, '');
  var c = b.replace(/pic\.(\w+|\.+|\/)+/g, '');
  // var c = b.replace(/pic(\w+)/g, '');
  return c;
}