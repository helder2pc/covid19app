
import React, { useState, useEffect } from 'react';
import {Modal, Text,SafeAreaView, StyleSheet,TouchableHighlight,Dimensions, View, Alert} from 'react-native';
import { WebView } from 'react-native-webview';
import Icon from 'react-native-vector-icons/Ionicons';
import { CountryDetailScreen } from '../screens';

// export default class ModalExample extends Component {
export default  DetailCountryModal =props => { 

   let content = (
      <View style={{flex:1}} >
        <Modal
        style={{flex:1}}
          animationType="slide"
          transparent={false}
          visible={props.modalVisible}
          startInLoadingState={true}
          onRequestClose={() => {
            props.setModalVisible(false);
          }}>

            <SafeAreaView  style={{flex:1}}>
              <TouchableHighlight
              style={{marginTop:0}}
                onPress={() => {
                  props.setModalVisible(!props.modalVisible);
              
                }}>
                  <Text style={styles.close}>Fechar</Text>
                {/* <Icon name="md-close-circle-outline" size={20} color="gray"></Icon> */}
              </TouchableHighlight>
           
                <CountryDetailScreen countryName={props.countryDetail} ></CountryDetailScreen>
   
            </SafeAreaView>

        </Modal>

      </View>
    );

    return content;
  
}

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

const styles = StyleSheet.create({
    webview: {
        flex: 1,
        // backgroundColor: 'yellow',
        width: deviceWidth,
        height: deviceHeight
    },
    close:{
      textAlign:"right",
      marginRight:10,
      marginBottom:5,
    }
});