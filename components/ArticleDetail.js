import React, { useState, useEffect } from 'react';
import { Modal, Text, Image, ActivityIndicator, TouchableOpacity, ScrollView, StyleSheet, TouchableHighlight, Dimensions, View, Alert } from 'react-native';
import { SafeAreaView } from 'react-navigation';
import HTML from 'react-native-render-html';
import I18n from '../I18n';

import { useHttp } from './../hooks/http';
import TitleHeader from './TitleHeader';
import { openLink } from './FunctionAux';


const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

// export default class ModalExample extends Component {
export default ArticleDetail = ({ navigation, articleId,setModalVisible }) => {
  const id= (articleId)?articleId:navigation.state.params.id;

  let body = `{\"query\":\"query {\\n  Article(\\n    where: { id: \\\"${id}\\\" }\\n  ) {\\n    date\\n    title\\n    content\\n    language\\n    source\\n    url\\n    imageUrl\\n  }\\n}\\n\"}`;
  const [isLoading, fetchedData] = useHttp(body, [], 'article', id);

  let content = (<View style={styles.containerIsLoading}><ActivityIndicator size="large" color="#3f6f9f" /></View>);

  if (!isLoading && fetchedData) {

    const styles = {
      img: { resizeMode: 'cover' },
    }

    const tagsStyles = {
      p: { fontSize: 16, },
      h2: { marginLeft: 10, marginRight: 10,  color: '#303030', },
      li: { fontSize: 16, letterSpacing: 0.8, }
    };

    const classesStyles = {
      'row': { padding: 10, },
      'bg-white': {
        fontSize: 16,
        paddingVertical: 10,
        letterSpacing: 0.8,
        lineHeight: 21,
      }
    }

    const renderers = {
      img: (htmlAttribs, children, passProps) => {
        return (
          <View key={`${htmlAttribs}view`} style={{ flexDirection: 'row', justifyContent: "center", alignItems: "center", paddingHorizontal: 1 }}>
            <Image
              key={htmlAttribs.src}
              source={{ uri: htmlAttribs.src, width: 90, height: 90 }}
              // style={{marginLeft:10}}
              // style={passProps.htmlStyles.img}
              {...passProps} />
            <View style={{ backgroundColor: '#007c91', flex: 1, height: 10, marginLeft: -2, marginRight: 10 }}></View>
          </View>
        );
      },
      p: (htmlAttribs, children, convertedCSSStyles, passProps) => {
        return (
          <Text style={{
            paddingVertical: 10,
            letterSpacing: 0.8,
            lineHeight: 21,
          }}>{children}</Text>
        );
      }
    };

    content = (
     <SafeAreaView style={{ flex: 1, }}>
      <ScrollView >
        <TitleHeader 
         back 
         navigation={navigation} 
         title={fetchedData.title}
         setModalVisible={setModalVisible}
         />
        <HTML

          html={fetchedData.content}
          htmlStyles={styles}
          renderers={renderers}
          tagsStyles={tagsStyles}
          classesStyles={classesStyles}
          imagesMaxWidth={Dimensions.get('window').width}
        />
        { (fetchedData && fetchedData.source) && <Text onPress={()=> openLink(fetchedData.url)} style={{ color: "grey", marginLeft: 20, marginTop: 10 }}>{`${I18n.t(`simpleText.${'source'}`)}: ${fetchedData.source}`}</Text>}
      <View style={{  height: 50 }}></View>
      </ScrollView>

      </SafeAreaView>

    );

  } else if (!isLoading && (!fetchedData)) {
    content = (<View style={styles.containerIsLoading}><Text>Could not fetch any data.</Text></View>);
  }
  return content;

}

const styles = StyleSheet.create({

  container: {
    flex: 1,
    paddingHorizontal: 10,
  },

  webview: {
    flex: 1,
    // backgroundColor: 'yellow',
    width: deviceWidth,
    height: deviceHeight
  },
  close: {
    textAlign: "right",
    marginRight: 10,
    marginBottom: 5,
  },

  webview: {
    flex: 1,
    // backgroundColor: 'yellow',
    width: deviceWidth,
    height: deviceHeight
  },
  close: {
    textAlign: "right",
    marginRight: 10,
    marginBottom: 5,
  },

  headerBoxBack: {
    width: 40,
    height: 60,
    // backgroundColor:'red',
    position: "absolute",
    left: -10,
    top: -20,
  },

  titleWrapper:{
      paddingVertical:70,
      paddingHorizontal:10,
  },

  containerIsLoading: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f5f5f5'
  },

  titleText:{
    fontSize:30,
}
});