
import React, { useState, useEffect } from 'react';
import {Modal, Text,SafeAreaView, StyleSheet,TouchableHighlight,Dimensions, View, Alert} from 'react-native';
import { WebView } from 'react-native-webview';
import Icon from 'react-native-vector-icons/Ionicons';
import I18n from '../I18n';

// export default class ModalExample extends Component {
export default  WebViewModal =props => { 

  //  alert(props.url);
   let content = (
      <View style={{flex:1}} >
        <Modal
        style={{flex:1}}
          animationType="slide"
          transparent={false}
          visible={props.modalVisible}
          startInLoadingState={true}
          onRequestClose={() => {
            props.setModalVisible(false);
          }}>

            <SafeAreaView  style={{flex:1}}>
              <TouchableHighlight
              style={{marginTop:0}}
                onPress={() => {
                  props.setModalVisible(!props.modalVisible);
                  if(props.setUrl)
                    props.setUrl(null);

                }}>
                  <Text style={styles.close}>{I18n.t("close").toUpperCase()}</Text>
                {/* <Icon name="md-close-circle-outline" size={20} color="gray"></Icon> */}
              </TouchableHighlight>

           
                <WebView
                  style={styles.webview}
                  source={{uri: props.url}}
                  javaScriptEnabled={true}
                  mediaPlaybackRequiresUserAction={true}
                  domStorageEnabled={true}
                  startInLoadingState={true}
                  scalesPageToFit={true} />
   

            </SafeAreaView>

        </Modal>

      </View>
    );

    return content;
  
}

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

const styles = StyleSheet.create({
    webview: {
        flex: 1,
        // backgroundColor: 'yellow',
        width: deviceWidth,
        height: deviceHeight
    },
    close:{
      textAlign:"right",
      marginRight:10,
      marginBottom:5,
    }
});