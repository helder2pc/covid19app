import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';

const CardExplore =props => {  
  // const[totalData] = props;
    return (
      <View style={styles.container}>
        <Text style={styles.exploreText}>Explore</Text>
        <View style={styles.mainRow}>

        <View style={styles.mainRow1}>

          <Text style={styles.itemText}>Overview</Text>
        </View>
        
        <View style={styles.mainRow2}>
          <View style={styles.rowItem}>
              <View style={[styles.itemBox, styles.itemBox1]}>
                <Text style={styles.itemText}>Prevention</Text>
              </View>

              <View style={[styles.itemBox, styles.itemBox2]}>
                <Text style={styles.itemText}>Treatment</Text>
              </View>
           </View>

          
           <View style={styles.rowItem}>
             
              <View style={[styles.itemBox, styles.itemBox3]}>
                <Text style={styles.itemText}>Symptoms</Text>
              </View>

              <View style={[styles.itemBox, styles.itemBox4]}>
                <Text style={styles.itemText}>Transmission</Text>
              </View>
           </View>

        </View>
        </View>
      </View>
    )
}

const styles = StyleSheet.create({
  container: {
    margin:10,
    borderRadius:16,
  },

  mainRow:{
    flexDirection:"row",
  },

  mainRow1:{
    width:"30%",
    alignItems:"center",
    justifyContent:"center",
    backgroundColor:'tomato',
    borderRadius:4,
  },

  mainRow2:{
    width:"70%",
  },

  rowItem:{
      flexDirection:"row",
      
      // alignItems:"center",
      // justifyContent:"space-evenly",
      // width:"50%"
  },

  itemBox:{
    flex:1,
    alignItems:"center",
    paddingVertical:15,
    margin:5,
    borderRadius:4,
  },

  itemBox1:{
    backgroundColor:"#87CEFA"
    
  },

  itemBox2:{
    backgroundColor:"#90EE90"
  },

  itemBox3:{
    backgroundColor:"#FA8072"
  },

  itemBox4:{
    backgroundColor:"#FFA500"
  },
  itemText:{
    color:"white",
  },
  exploreText:{
    marginBottom:10,
    fontSize:30,
    fontWeight:"bold",
  }

  
});

export default CardExplore;