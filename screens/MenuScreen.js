import React, { useState } from 'react';
import { View, Platform, Linking, Text, SafeAreaView, StyleSheet, TouchableOpacity, ScrollView } from 'react-native';
import Share from 'react-native-share';
import FadeInView from '../components/FadeInView';
import Icon from 'react-native-vector-icons/FontAwesome5';
import WebViewModal from '../components/WebViewModal';
import TitleHeader from '../components/TitleHeader';
import I18n from '../I18n';
import { isLanguagePT } from '../components/FunctionAux';
import DetailArticleModal from '../components/DetailArticleModal';
import AboutUsScreen from './AboutUsScreen';


const GOOGLE_PACKAGE_NAME = 'com.cofeed19';
const APPLE_STORE_ID = '1504008628';

const url = 'http://kriolmidia.com/cofeed19';
const title = 'CoFeed19';
const message = I18n.t("share.message");
const options = Platform.select({
  ios: {
    activityItemSources: [
      {
        placeholderItem: { type: 'url', content: url },
        item: {
          default: { type: 'url', content: url },
        },
        subject: {
          default: title,
        },
        linkMetadata: { originalUrl: url, url, title },
      },
      {
        placeholderItem: { type: 'text', content: message },
        item: {
          default: { type: 'text', content: message },
          message: null, // Specify no text to share via Messages app.
        },
      },
    ],
  },
  default: {
    title,
    subject: title,
    message: `${message} ${url}`,
  },
});

export default MenuScreen = props => {

  const [modalVisible, setModalVisible] = useState(false);
  const [itemToShow, setItemToShow] = useState(null);

  var pkg = require('./../package.json');

  let content = (
    <SafeAreaView>

      <FadeInView>
        <ScrollView  >

          <TitleHeader title={'Menu'} />

          

          <TouchableOpacity
            style={styles.listItem}
            onPress={() => {
             
              setItemToShow('aboutus')
              setModalVisible(true);
    

            }}
          >
            <Icon style={{ marginRight: 15 }} name="users" size={20} color={"grey"} />
            <Text style={styles.listItemText}>{I18n.t("title.aboutus")}</Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.listItem}
            onPress={() => {
              if (Platform.OS != 'ios') {
                //To open the Google Play Store
                Linking.openURL(`market://details?id=${GOOGLE_PACKAGE_NAME}`).catch(err =>
                  alert('Please check for the Google Play Store')
                );
              } else {
                //To open the Apple App Store
                Linking.openURL(
                  `itms://itunes.apple.com/in/app/apple-store/${APPLE_STORE_ID}`
                ).catch(err => alert('Please check for the App Store'));
              }
            }}
          >
            <Icon style={{ marginRight: 15 }} name="star" size={20} color={"grey"} />
            <Text style={styles.listItemText}>{I18n.t("title.rateus")}</Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.listItem}
            onPress={() => {
              Share.open(options);
            }}
          >
            <Icon style={{ marginRight: 15 }} name="share-alt" size={20} color={"grey"} />

            <Text style={styles.listItemText}>{I18n.t("title.shareus")}</Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.listItem}
            onPress={() => {

              setItemToShow('privacyAndPolicy')
              setModalVisible(true);
              
            }}
          >
            <Icon style={{ marginRight: 15 }} name="readme" size={25} color={"grey"} />
            <Text style={styles.listItemText}>{I18n.t("title.privacyAndPolicy")}</Text>
          </TouchableOpacity>


          <TouchableOpacity
            style={styles.listItem}
            onPress={() => {}}
          >
            <Icon style={{ marginRight: 15 }} name="whmcs" size={25} color={"grey"} />
          <Text style={styles.listItemText}>{I18n.t("simpleText.version")} - {pkg.version}</Text>
          </TouchableOpacity>





  <DetailArticleModal 
     modalVisible={modalVisible} 
    setModalVisible={setModalVisible}
      >
     { (itemToShow=='aboutus') && <AboutUsScreen
    setModalVisible={setModalVisible} />}
  
    { (itemToShow=='privacyAndPolicy') && <ArticleDetail
               articleId={(isLanguagePT())?'5e75dec8ec653d002105696a':'5e75dceaec653d0021056969'}
    setModalVisible={setModalVisible}
              />}
  
  </DetailArticleModal>
        </ScrollView>
      </FadeInView>
    </SafeAreaView>
  );


  // if (modalVisible)
  //   content = (
  //     <WebViewModal
  //       modalVisible
  //       setModalVisible={setModalVisible}
  //       url={'http://kriolmidia.com/'}

  //     ></WebViewModal>
  //   );
  return content;

}


const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  containerIsLoading: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },

  headerWrapper: {
    paddingVertical: 30,
    paddingHorizontal: 10,
    flex: 1,
    backgroundColor: "rgba(63, 111, 159, 0.9)",
  },

  headerText: {
    fontSize: 30,
    color: "rgba(255, 255, 255, 0.95)",
    fontFamily: "BebasNeue",
    // textAlign:"center",
  },

  listItem: {
    paddingTop: 30,
    paddingBottom: 15,
    marginHorizontal: 15,
    borderBottomColor: '#E8E8E8',
    borderBottomWidth: 1,
    flexDirection: "row",
    alignItems: "center"
  },

  listItemText: {
    fontSize: 18,
    color: "grey",
    // fontFamily:"BebasNeue"
    // borderWidth:1,

  }


});



