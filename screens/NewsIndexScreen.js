
import React, { useState, useEffect } from 'react';
import { View, SafeAreaView, ActivityIndicator, Dimensions, StyleSheet, Text, FlatList } from 'react-native';
import * as rssParser from 'react-native-rss-parser';
import ItemNews from '../components/ItemNews';
import { WebView } from 'react-native-webview';
import WebViewModal from '../components/WebViewModal';
const NewsIndexScreen = props => {


  const [loadedChars, setLoadedChars] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [modalVisible, setModalVisible] = useState(false);
  const [url, setUrl] = useState(null);


  let content = (<View style={styles.containerIsLoading}><ActivityIndicator size="large" color="#3f6f9f" /></View>);


  if (!isLoading && loadedChars && loadedChars.length > 0  ) {
    content = (
      <SafeAreaView style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>

        {/* {loadedChars && <FlatList
          data={loadedChars}
          renderItem={({ item }) => <ItemNews data={item} setUrl={setUrl} setModalVisible={setModalVisible}/>}
          keyExtractor={item => item.id}
        />} */}
      </SafeAreaView>
    );
  } else if (!isLoading && (!loadedChars || loadedChars.length === 0) ) {
    content = (<View style={styles.containerIsLoading}>
      <Text>Could not fetch any data.</Text>
    </View>);
  }

if(modalVisible)
  content = (  
    <WebViewModal  
      modalVisible 
      setModalVisible={setModalVisible}
      url={url} 
      setUrl={setUrl}
    ></WebViewModal>
  );



  return content;
}

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

const styles = StyleSheet.create({
  webview: {
    flex: 1,
    // backgroundColor: 'yellow',
    width: deviceWidth,
    height: deviceHeight
  },
  container: {
    flex: 1,
    backgroundColor: '#f5f5f5'
  },
  containerIsLoading: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f5f5f5'
  },
});

export default NewsIndexScreen;