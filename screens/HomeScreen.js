import React, { useState, useEffect } from 'react';
import { View, Image, KeyboardAvoidingView, RefreshControl, ActivityIndicator, ScrollView, Text, StyleSheet, SafeAreaView, FlatList, TextInput, TouchableOpacity } from 'react-native';
import I18n from '../I18n';
import FadeInView from '../components/FadeInView';
import OverviewTabContent from '../components/OverviewTabContent';
import { isLanguagePT } from '../components/FunctionAux';
import NetInfo from "@react-native-community/netinfo";
import Toast from 'react-native-simple-toast';


export default HomeScreen = props => {



  // let body = `{\"query\":\"query {  allEntries(    where: { latest: true }     orderBy: \\\"date_DESC\\\"  ) {    date   country {      name  code    lat      lng    }    newCases    newDeaths    totalCases cured  critical totalDeaths  }}\"}`;
   const [refresh, setRefresh] = useState(false);
   const [refreshing, setRefreshing] = useState(false);




  const [tabTopValue, setTabTopValue] = useState('');

  const goArticles = value =>{

    props.navigation.push( 
      'Articles',
      {
        id: value,
        
      }
      );

  }


 
    let content = (
      
      <SafeAreaView style={styles.container}>

{/* keyboardVerticalOffset={400} */}
        <ScrollView keyboardShouldPersistTaps={'always'}
        
        refreshControl={
          <RefreshControl
           refreshing={refresh}
            onRefresh={()=>{
              NetInfo.fetch().then(state => {
                if(state.isConnected)
                    setRefreshing(true);
                else{
                  Toast.showWithGravity(I18n.t(`internetError`), Toast.LONG, Toast.TOP);         
  
                }
                })
             
            }} 
           tintColor="#3f6f9f"
          />
        }
        >

       

          <FadeInView>
            {/* <CardExplore></CardExplore> */}



            {/* <View style={styles.dateContainer}>
             <Text style={styles.dateContainerText}>Mar 15, 2020</Text>
           </View> */}

            <View style={styles.titleHeaderImage}>
              <Image style={{ width: 70, height: 70 }} source={require('./../assets/imgs/logo.png')} />

             </View>
            <View style={styles.tabTopContainer}>


            <TabHeader
                text={I18n.t("home.selfassessment")}
                onPress={() => {
                  props.navigation.navigate(
                  "SelAssessment"
                  )
                }}
                tabTopValue={tabTopValue}
                value={"overview"}
                iconPath={require('./../assets/imgs/overview.png')}
              />

              <TabHeader
                text={I18n.t("home.symptoms")}
                value={"symptoms"}
                onPress={() => { goArticles((isLanguagePT())?'5e72adbaadf7320021e301ae':"5e72a9c2adf7320021e301ad") }}
                tabTopValue={tabTopValue}
                iconPath={require('./../assets/imgs/symptoms.png')}
              />


              <TabHeader
                text={I18n.t("home.prevention")}
                value={"prevention"}
                onPress={() => { goArticles((isLanguagePT())?'5e733dce178c3e0021a4bd40':'5e72af93adf7320021e301af') }}
                tabTopValue={tabTopValue}
                iconPath={require('./../assets/imgs/prevention.png')}
              />


              <TabHeader
                text={I18n.t("home.transmission")}
                value={"transmission"}
                onPress={() => { goArticles((isLanguagePT())?'5e73504de92a4d002143dafc':'5e73412f178c3e0021a4bd41') }}
                tabTopValue={tabTopValue}
                iconPath={require('./../assets/imgs/transmission.png')}
              />



           
              {/* <TouchableOpacity
                style={[styles.tabTopBox, { backgroundColor: "#194A72" }]}
                setTabTopValue
                onPress={() => { setTabTopValue('overview') }}
              >
                <Image style={{ width: 60, height: 60 }} source={require('./../assets/imgs/symptoms.png')} />
                <Text style={styles.tabTopText} >Overview</Text>
              </TouchableOpacity> */}

            </View>
            <View style={styles.tabContent}>
           {  <OverviewTabContent setRefreshing ={setRefreshing} refreshing={refreshing} refresh={refresh} navigation={props.navigation} />}
         
         </View>


      

          </FadeInView>

        </ScrollView>

      </SafeAreaView>
    );
  return content;
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f5f5f5'
  },
  containerIsLoading: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f5f5f5'
  },
  dateContainer: {
    marginTop: 20,
    alignItems: 'center',
  },
  dateContainerText: {
    opacity: 0.7,
    fontSize: 13,
  },

  mainContainer: {

    marginHorizontal: 10,
    borderRadius: 16,
    backgroundColor: "rgba(63, 111, 159, 0.2)",
  },
  textInputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: "center",
    //backgroundColor: "#FFFAF0",
    borderColor: 'gray',
    borderWidth: 1,
    marginHorizontal: 15,
    paddingHorizontal: 10,
    borderRadius: 10,
    marginVertical: 15,
  },

  textInput: {
    height: 40,
    // width:'70%',
    flex: 1,
    // borderColor: 'gray',
    //  borderWidth: 1,
    marginHorizontal: '1%',
    paddingHorizontal: 5,
    //  borderRadius:10,
    marginVertical: 5,

  },



  tabTopBox: {
    width: "25%",
    // padding: 20,
    paddingVertical: 15,
    borderRadius: 8,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "transparent",
  },

  tabTopBoxSelected: {
    width: "31%",
    // padding: 20,
    paddingVertical: 15,
    borderRadius: 8,
    justifyContent: "center",
    alignItems: "center",

    backgroundColor: "#194A72",

  },
  tabTopText: {
    // color: "white",
    textTransform: "uppercase",
    fontSize: 14,
    marginTop: 5,
    fontFamily: "BebasNeue",
  },

  tabTopContainer: {
    flexDirection: "row",
    paddingVertical: 0,
    paddingHorizontal: 10,
    backgroundColor: "rgba(63, 111, 159, 0.2)",
    paddingVertical: 25
  },

  titleHeaderImage: {
    justifyContent: "center",
    alignItems: "center",
    marginVertical: 8
  },

  tabContent: {
marginTop:20
  }

});


const TabHeader = props => {

  return (

    <TouchableOpacity
      style={[styles.tabTopBox, (props.tabTopValue == props.value) ? styles.tabTopBoxSelected : styles.tabTopBox]}
      setTabTopValue
      // onPress={() => { props.setTabTopValue('overview') }}
      onPress={props.onPress}
    >
      <Image style={{ width: 60, height: 60 }} source={props.iconPath} />
      <Text style={[styles.tabTopText, { color: (props.tabTopValue == props.value) ? "#fff" : "#3f6f9f" }]} >{props.text}</Text>
    </TouchableOpacity>

  )
}


