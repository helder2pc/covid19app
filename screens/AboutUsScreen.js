import React, { useState } from 'react';
import { View, Text, SafeAreaView, StyleSheet, ActivityIndicator, ScrollView } from 'react-native';
import FadeInView from '../components/FadeInView';
import TitleHeader from '../components/TitleHeader';
import I18n from '../I18n';
import { openInbox } from 'react-native-email-link';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { openLink } from '../components/FunctionAux';

export default AboutUsScreen = props => {
  


  let content = (<View style={styles.containerIsLoading}><ActivityIndicator size="large" color="#3f6f9f" /></View>);



  content = (
    <SafeAreaView style={{ flex: 1 }}>

      <FadeInView>
        <ScrollView  >
          <TitleHeader back title={I18n.t("title.aboutus")} setModalVisible={props.setModalVisible} />

          <View style={styles.textContainer}>

            <Text style={styles.text}>
              {I18n.t("aboutus.paragraph1")}
            </Text>

            <Text style={styles.text}>
              {I18n.t("aboutus.paragraph2")}
            </Text>
            <View>


            </View>
            <Text style={styles.text}>
              {I18n.t("aboutus.paragraph31")}  <Text
                // {I18n.t("aboutus.paragraph31")}  <Text onPress={() => openLink('support@example.com?subject=SendMail&body=Description') }
                // {I18n.t("aboutus.paragraph31")}  <Text onPress={() => openLink('mailto:hello@kriolmidia.com') }
                title="hello@kriolmidia.com" >
                <Text style={[styles.text]}>hello@kriolmidia.com</Text>
              </Text> {I18n.t("aboutus.paragraph32")}<Text 
                title="http://kriolmidia.com/" >
                <Text style={[styles.text, styles.textLink]} onPress={() => openLink('http://kriolmidia.com/')}>http://kriolmidia.com/</Text>
              </Text>
            </Text>

            <View style={styles.socailMediaContent}>

            <View style={styles.socailMediaItemWrapper}>
            <Icon name="instagram" size={25} color="#C13584" />
             <Text style={[styles.text, styles.socailMediaItemText, styles.textLink]}
                onPress={() => openLink('https://www.instagram.com/kriolmidia/')}>@kriolmidia
             </Text>
            </View>

            <View style={styles.socailMediaItemWrapper}>
            <Icon name="facebook" size={25} color="#194A72" />
             <Text style={[styles.text, styles.socailMediaItemText, styles.textLink]}
                onPress={() => openLink('https://web.facebook.com/Kriol-Midia-137563366869750/')}> Kriol Midia
             </Text>
            </View>

            <View style={styles.socailMediaItemWrapper}>
            <Icon name="youtube" size={25} color="#FF0000" />
             <Text style={[styles.text, styles.socailMediaItemText, styles.textLink]}
                onPress={() => openLink('https://www.youtube.com/channel/UCM243WWiu7D13SP2_CSrODg?view_as=subscriber')}>Kriol Midia
             </Text>
            </View>

            
            </View>
          </View>
        </ScrollView>
      </FadeInView>
    </SafeAreaView>
  );


  return content;

}


const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  containerIsLoading: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },

  text: {
    fontSize: 17,
    marginVertical: 10,
    color: '#282828',
    lineHeight: 24,
    letterSpacing: 0.8
  },

  textLink: {
    color: "rgba(63, 111, 159, 0.8)",
    fontWeight: "bold",
  },

  textContainer: {
    paddingVertical: 25,
    paddingHorizontal: 15,

  },
  socailMediaContent:{
    marginVertical:20,
  },

  socailMediaItemWrapper:{
      flexDirection:"row",
      alignItems: "center"
  },
  socailMediaItemText:{
    fontSize:18, 
    color:'#282828',
    marginLeft:10
  }

});



