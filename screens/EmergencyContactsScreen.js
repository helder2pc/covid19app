import React,{useState} from 'react';
import { View, Text, SafeAreaView,Dimensions,  TextInput, StyleSheet, ActivityIndicator, TouchableOpacity, ScrollView, Linking, Platform } from 'react-native';
import FadeInView from '../components/FadeInView';
import Icon from 'react-native-vector-icons/FontAwesome';
import { useHttp } from '../hooks/http';
import Flag from 'react-native-flags';
 import { callNumber, searchInput } from '../components/FunctionAux';
import TitleHeader from '../components/TitleHeader';
import AsyncStorage from '@react-native-community/async-storage';
import I18n from '../I18n';

export default EmergencyContactsScreen = props => {


  let body = "{\"query\":\"query {\\n  allEmergencyNumbers {\\n    country {\\n      code\\n      name\\n    }\\n    title\\n    description\\n    phone\\n  }\\n}\\n\"}";
  const [isLoading, fetchedData] = useHttp(body, [], 'allEmergencyNumbers');
  const [data, setData] = useState(null);
 
 
  if (!isLoading && fetchedData && fetchedData.length > 0 && data==null) {
    


    // setData(fetchedData);
        


    // AsyncStorage.getItem('favoritesCountry').then(
 
       
    //   result => {
    //     //  alert(result)
    //     let favotites = [];
    //     if(result){
    //       favotites= JSON.parse(result);
    //     }
       
    //     const list=[];

    //     fetchedData.forEach(item=>{
    //       console.log(item);
    //       if(favotites.filter(name => name ==item.country.name).length==0)
    //           {
    //             item.isFavorite=false;
              
    //           }
    //       else{
    //         // alert(favotites)
    //         item.isFavorite=true;
    //       }
              

    //           list.push(item);
    //     });

    //     //list.sort((b, a) => (a.isFavorite > b.isFavorite) ? 1 : -1);
    //     list.sort((a, b) => (b.isFavorite > a.isFavorite) ? 1 : (a.isFavorite === b.isFavorite) ? ((a.country.name > b.country.name) ? 1 : -1) : -1 )

    //     setData(list);
       
    //   }
    // );

    let list =[]
    fetchedData.forEach(element => {
      element.country.countryName = I18n.t(`countries.${element.country.code}`);
      list.push(element)
    });
    list.sort((a, b) => (a.country.name > b.country.name) ? 1 : -1);

    setData(list);

  
  }




    
   // data.sort((a, b) => (a.country.name > b.country.name) ? 1 : -1)

    let content = (
      <SafeAreaView style={{ flex:1}}>

        <FadeInView   >
          <ScrollView >

    
            <TitleHeader   title={I18n.t("title.emergencyContact")}/>
            {
              (isLoading) && <>
              <View style={styles.containerIsLoading}>
              <ActivityIndicator size="large" color="#3f6f9f" />
              </View>
              </>
            }
            
            {(!isLoading && fetchedData && fetchedData.length > 0 && data) &&<>
            
            <View style={styles.textInputContainer}>
              <Icon name="search" size={18} color="grey" />
                <TextInput
                style={styles.textInput}
                  onChangeText={(text) => {
                  //   props.onChangeText(text);
                    setData(searchInput(text,  fetchedData))
                  }}
                  // value={value}
                  placeholder={I18n.t("searchPlaceholder")}
                />
             </View>
             
             
             <View style={styles.listWrap}> 

                { 
                  data.map((data) => {
                    return (
                    
                      
                          <TouchableOpacity 
                              key={data.country.name}
                              style={styles.itemWrap}
                              onPress={() => {
                                callNumber(data.phone);
                                  }}
                            >
                              {/* { (data.isFavorite) && <Icon  style={{position:"absolute",  right:10, top:5}} name={'star'} size={15} color={"#3f6f9f"} />} */}

                                  <Flag code={data.country.code} size={64}/>
                                    <Text style={styles.itemText} >{data.country.countryName}</Text>
                            
                            
                          </TouchableOpacity>
                    
                    )
                  })
                }

          </View>
            </>
            }
        {
          // (!isLoading && (!fetchedData || fetchedData.length === 0 &&  !data)) &&
          // <View style={styles.containerIsLoading}><Text>Could not fetch any data.</Text></View>
        }
          
          </ScrollView>
        </FadeInView>
      </SafeAreaView>
    );
 

 

    return content;

}


const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  containerIsLoading: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },

  headerWrapper:{
    paddingVertical:30,
    paddingHorizontal:10,
    flex:1,
    backgroundColor: "rgba(63, 111, 159, 0.9)",
  },

  headerText:{
    fontSize:30,
    color:"rgba(255, 255, 255, 0.9)",
    fontFamily:"BebasNeue",
    // textAlign:"center",
  },

  listWrap:{
    flexDirection:'row',
    flexWrap:'wrap',
    padding:"2.5%",
    // justifyContent:"space-evenly",
    // alignItems:"center"
    
  },

  itemWrap:{

    backgroundColor:"rgba(63, 111, 159, 0.3)",
    width:(Dimensions.get('window').width>420)?'30.3%':'44.5%',
    alignItems:"center",
    paddingHorizontal:"1.5%",
    paddingVertical:"2.5%",
    marginVertical:"2.5%",
    marginHorizontal:"2.5%",
    borderRadius:4,    
  },

  itemText:{
    fontFamily:"BebasNeue",
    color:"#3f6f9f",
    fontSize:16,
  },

  textInputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: "center",
    backgroundColor: "#FFFAF0",
    borderColor: 'gray',
    borderWidth: 1,
    marginHorizontal: 15,
    paddingHorizontal: 10,
    paddingVertical: Platform.OS === 'ios' ? 15 : 0,
    borderRadius: 10,
    marginVertical: 15,
    // width:'85%',
  },

  textInput: {
    color: "black",
    // width:'70%',
    flex: 1,
    // borderColor: 'gray',
    //  borderWidth: 1,
    paddingHorizontal: 5,
    //  borderRadius:10,

  },


  
});


