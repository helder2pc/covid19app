import * as React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { color } from 'react-native-reanimated';
import { white } from 'color-name';
 
export const MyCluster = (props) => {
  const { count } = props;
  return (
    <View style={[styles.container,
        { width: getSize(count), 
            height: getSize(count), 
            borderRadius: 
            getSize(count)/2,
            backgroundColor: (props.color)?props.color:'tomato',    }]}>
      <Text style={styles.text}>{count}</Text>
    </View>
  )
}
 

const styles = StyleSheet.create({
    container: {
        
        borderRadius: 30,
        
        justifyContent: 'center',
        alignItems: 'center',
        opacity:0.8
    },
    text:{
        color: 'white', 
    } 
})

const getSize = size =>{
  if(size<=9)
    return 20;
  if(size>9 && size<=99)
  return 25;

  if(size>99 && size<=999)
  return 35;

  if(size>999 && size<=9999)
  return 45;

  if(size>9999 && size<=99999)
  return 50;

  if(size>99999 && size<=999999)
  return 65;

  return 80;

}