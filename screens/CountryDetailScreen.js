import React, { useState, useEffect } from 'react';
import { View, SafeAreaView, Switch, Text, ActivityIndicator, StyleSheet, TouchableOpacity, FlatList, Dimensions } from 'react-native';
import { useHttp } from './../hooks/http';
import CardHeader from '../components/CardHeader';
import ItemList from '../components/ItemList';
import moment from "moment";
import Icon from 'react-native-vector-icons/FontAwesome';
import Flag from 'react-native-flags';
import I18n from '../I18n';

import {
  LineChart,
} from "react-native-chart-kit";
import FadeInView from '../components/FadeInView';
import AsyncStorage from '@react-native-community/async-storage';
import { ScrollView } from 'react-native-gesture-handler';
import { formatDate, isLanguagePT } from '../components/FunctionAux';

const CountryDetailScreen = ({ route, navigation }) => {

  const { countryName, date, code, isFavorite, setUpdated } = navigation.state.params;

  // let date = '2020-03-14';
  // let body = `{\"query\":\"query {  allEntries(    where:{date:\\\"${date}\\\"}    orderBy: \\\"date_DESC\\\"  ) {    date   country {      name      lat      lng    }    newCases    newDeaths    totalCases    totalDeaths  }}\"}`;
  let body = `{\"query\":\"query {\\n  allEntries(\\n    where: { country: { code: \\"${code}\\" } },\\n    orderBy: \\\"date_DESC\\\"\\n  ) {\\n    date    newCases   newDeaths   totalCases cured  critical  totalDeaths }\\n}\\n\"}`;
  const [isLoading, fetchedData] = useHttp(body, []);

  const [totalData, setTotalData] = useState(null);
  const [totalCases, setTotalCase] = useState(null);
  const [showList, setShowList] = useState(false);
  const [favorite, setFavorite] = useState(null);
  const [switchIsConfirmed, setswitchIsConfirmed] = useState(false);




  if (fetchedData && fetchedData.length > 0) {


    let total = {
      "newCases": 0,
      "newDeaths": 0,
      "totalCases": 0,
      "totalDeaths": 0,
      "totalCured": 0,
      "totalCritical": 0,

    };
    let lastdata = fetchedData[0];
    //onsole.log('lastdata');
    //onsole.log(lastdata);
    if (lastdata.newCases)
      total.newCases += lastdata.newCases;
    if (lastdata.newDeaths)
      total.newDeaths += lastdata.newDeaths;
    if (lastdata.totalCases)
      total.totalCases += lastdata.totalCases;
    if (lastdata.totalDeaths)
      total.totalDeaths += lastdata.totalDeaths;
    if (lastdata.cured)
      total.totalCured += lastdata.cured;
    if (lastdata.critical)
      total.totalCritical += lastdata.critical;

    //onsole.log(total);

    if (!totalData)
      setTotalData(total);


    let totalArray = {
      "dates": [],
      "newCases": [],
      "newDeaths": [],
      "totalCases": [],
      "totalDeaths": [],
      "totalCured": [],
      "totalCritical": [],
    };

    for (let i = 0; i < fetchedData.length; i++) {

      let element = fetchedData[i];
      totalArray.dates.push(dateFormat(element.date));

      totalArray.newCases.push((element.newCases) ? element.newCases : 0);

      totalArray.newDeaths.push((element.newDeaths) ? element.newDeaths : 0);

      totalArray.totalCases.push((element.totalCases) ? element.totalCases : 0);

      totalArray.totalDeaths.push((element.totalDeaths) ? element.totalDeaths : 0);

      totalArray.totalCritical.push((element.critical) ? element.totalCritical : 0);

      totalArray.totalCured.push((element.cured) ? element.totalCured : 0);

      if (i == 6)
        break;
    };

    if (!totalCases) {
      setTotalCase(totalArray);
      if (favorite == null)
        setFavorite(isFavorite);
    }



  }

  let content = (
    <View style={styles.container}>

<View style={{flex:1}}>
      <View style={{flex:1, justifyContent:"center"}}>
          <ActivityIndicator size="large" color="white" />
      </View>
      </View>
    </View>);

  if (!isLoading && fetchedData && fetchedData.length > 0) {

    content = (
      <SafeAreaView style={styles.container}>
        <ScrollView>
          <FadeInView style={styles.headerContainer}>

            <View style={styles.headerBox}>
              <TouchableOpacity
                style={{ flex: 0}}
                onPress={() => {
                  navigation.pop();
                }}
              >
                <View hitSlop={{top: 50, bottom: 50, left: 50, right: 50}} style={[styles.headerBoxBack,{zIndex:3}]}>
                  <Icon style={{ paddingTop: 10, marginLeft: 10 }} name="angle-left" size={25} color={"white"} />
                </View>

              </TouchableOpacity>

                <View>
           {code && <View style={styles.countryNameBox}>
              <View style={styles.countryNameCodeBox}>
                <Flag code={code} size={32}></Flag>
                <Text style={styles.countryNameText}>{I18n.t(`countries.${code}`)}</Text>
              </View>
              <Text style={styles.dateText}>{formatDate(new Date(date),'LLL')}</Text>
            </View>}
            </View>
              <TouchableOpacity
                style={{ flex: 0}}
                onPress={() => {

                  //AsyncStorage.removeItem('favoritesCountry').then()
                  AsyncStorage.getItem('favoritesCountry').then(

                    result => {
                      
                      let listNames = [];

                      if (result) {

                        listNames = JSON.parse(result);

                        let arrayFilter = listNames.filter(_code => _code == code);
                        if (arrayFilter.length == 0) {
                          listNames.push(code);
                          setFavorite(true);

                        }
                        else {
                          // listNames.remove(countryName);
                          let index = listNames.indexOf(code);
                          if (index > -1) {
                            listNames.splice(index, 1);
                          }


                          setFavorite(false);
                        }
                      }

                      else {

                        // alert()
                        listNames.push(code);
                        setFavorite(true);
                      }

                      AsyncStorage.setItem('favoritesCountry', JSON.stringify(listNames)).then(
                        val => {
                          setUpdated(true);
                        }
                      )
                    }
                  )

                }}
              >
                <View hitSlop={{top: 50, bottom: 50, left: 50, right: 50}} style={[styles.headerBoxBack,{zIndex:3}]}>
                <Icon style={{ paddingTop: 10, marginLeft: 10 }} name={(favorite) ? 'star' : 'star-o'} size={25} color={"white"} />
                </View>
              </TouchableOpacity>

            </View>
          </FadeInView>

          {totalData && <CardHeader isDetail={true} totalData={totalData} />}

          <FadeInView style={styles.bodyContainer}>
            <View style={styles.containerSelect}>

              <TouchableOpacity style={[{ backgroundColor: (!showList) ? '#3F6F9F' : 'transparent' , borderColor: (!showList) ? '#3F6F9F' : 'grey' }, styles.containerSelectBox]} onPress={() => { setShowList(false) }}>

                <Icon name="line-chart" size={20} color={(!showList) ? "white" : "#3F6F9F"} />
              </TouchableOpacity>
              <TouchableOpacity style={[{ backgroundColor: (showList) ? '#3F6F9F' : 'transparent', borderColor: (!showList) ? '#3F6F9F' : 'grey' }, styles.containerSelectBox]} onPress={() => { setShowList(true) }}>
                <Icon name="list" size={20} color={(showList) ? "white" : "#3F6F9F"} />
              </TouchableOpacity>
            </View>


            <View style={styles.listContainer}>
              {fetchedData && showList && <FlatList

                data={fetchedData}
                renderItem={({ item }) => <ItemList data={item} isDetail={true} />}
                keyExtractor={item => item.date}
              />}
            </View>


            {totalCases && !showList && <View style={styles.chartContainer}>




              {/* <Text>Bezier Line Chart</Text> */}
              <LineChart
                data={{
                  labels: reverseArray(totalCases.dates),
                  datasets: [
                    {
                      data: (!switchIsConfirmed) ? reverseArray(totalCases.totalCases) : reverseArray(totalCases.totalDeaths), color: (switchIsConfirmed) ? (opacity = 1) => `rgba(32, 32, 32, ${opacity})` : (opacity = 1) => `rgba(255, 255, 255, ${opacity})`
                    },
                    // {
                    //   data: reverseArray(totalCases.newCases),color: (opacity = 1) => `rgba(32, 32, 32, ${opacity})`,
                    // }
                  ]
                }}
                width={Dimensions.get("window").width - 20} // from react-native
                height={270}
                yAxisLabel=""
                yAxisSuffix=""
                yAxisInterval={1} // optional, defaults to 1
                verticalLabelRotation={30}
                chartConfig={{
                  backgroundColor: "#3F6F9F",
                  // backgroundGradientFrom: "#fb8c00",
                  backgroundGradientFrom: "#3F6F9F",
                  // backgroundGradientTo: "#ffa726",
                  backgroundGradientTo: "#194A72",
                  decimalPlaces: 0, // optional, defaults to 2dp
                  color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                  labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                  style: {
                    borderRadius: 16
                  },
                  propsForDots: {
                    r: "6",
                    strokeWidth: "2",
                    stroke: "#ffa726"
                  }
                }}
                bezier
                style={{
                  marginVertical: 8,
                  borderRadius: 16
                }}
              />

              <View style={styles.switchContainer}>

                <View style={styles.switchBox}>
                  <Text style={styles.switchText}>{I18n.t("simpleText.confirmed")}</Text>
                  <Switch
                    onValueChange={() => { setswitchIsConfirmed(!switchIsConfirmed) }}
                    value={switchIsConfirmed}
                    trackColor={{ true: 'gray', false: '#3F6F9F' }}
                  />
                  <Text style={styles.switchText}>{I18n.t("simpleText.deaths")}</Text>
                </View>

              </View>
            </View>
            }
          </FadeInView>
        </ScrollView>
      </SafeAreaView>
    )
  }

  else if (!isLoading && (!fetchedData || fetchedData.length === 0)) {
    content = (<View style={styles.containerIsLoading}><Text>Could not fetch any data.</Text></View>);
  }
  return content;
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#3F6F9F",
  },

  containerIsLoading: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: '#f5f5f5'
    backgroundColor: "#3F6F9F",
  },

  headerBox: {
    flex: 1,
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingVertical: 20,
    paddingHorizontal: 15,
    // backgroundColor:"blue"
  },
  headerBoxBack: {
//    position: "absolute",
    width: 40,
    height: 60,
    //  backgroundColor:'red',
//    position: "absolute",
//    left: -10,
//    top: -20,
  },
  countryNameBox: {
    alignItems: "center",

  },
  bodyContainer: {
    backgroundColor: 'white',
    // borderTopRightRadius:25,
    borderTopLeftRadius: 25,
    height: '100%'
    // flex: 1
  },

  countryNameCodeBox: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",

  },

  countryNameText: {
    fontSize: 28,
    color: "#fff",
    fontFamily: "BebasNeue",
    margin:3
  },
  dateText: {
    fontSize: 14,
    marginTop: 5,
    marginBottom: 20,
    color: "#fff"
  },
  listContainer: {
    marginTop: 20,

  },
  chartContainer: {
    marginHorizontal: 10,
    // marginVertical:20,
  },

  containerSelect: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginTop: 30,

  },
  containerSelectBox: {
    borderRadius: 22,
    borderWidth: 0.5,
    paddingVertical: 12,
    paddingHorizontal: 50,
  },
  headerContainer: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  switchContainer: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 10,
    marginBottom: 5,
    marginLeft: -15,
  },
  switchBox: {
    alignItems: "center",
    marginHorizontal: 20,
    flexDirection: "row",
  },

  switchText: {
    paddingHorizontal: 10,
  },




});


const dateFormat = value => {
  try {
    if (value){
        
      return moment(new Date(value)).format("DD-MMM");
    } 
  }
  catch (error) {
  }
  return "-";
}

const reverseArray = array => {
  try {
    let list = [];
    for (let i = array.length - 1; i >= 0; i--)
      list.push(array[i]);
    return list

  }
  catch (error) {
  }
  return [];
}



export default CountryDetailScreen;