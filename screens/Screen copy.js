import React,{useState} from 'react';
import { View, Text, SafeAreaView, StyleSheet, ActivityIndicator, TouchableOpacity, ScrollView } from 'react-native';
import FadeInView from '../components/FadeInView';
import Icon from 'react-native-vector-icons/FontAwesome5';
import WebViewModal from '../components/WebViewModal';
import { useHttp } from '../hooks/http';


export default Screen = props => {

  let body = `{\"query\":\"query {  allEntries(    where: { latest: true }     orderBy: \\\"date_DESC\\\"  ) {    date   country {      name  code    lat      lng    }    newCases    newDeaths    totalCases cured  critical totalDeaths  }}\"}`;
  const [isLoading, fetchedData] = useHttp(body, []);
  const [modalVisible, setModalVisible] = useState(false);


  let content = (<View style={styles.containerIsLoading}><ActivityIndicator size="large" color="#3f6f9f" /></View>);

  if (!isLoading && fetchedData && fetchedData.length > 0) {

     content = (
      <SafeAreaView>

        <FadeInView>
          <ScrollView  >

            <View  style={styles.headerWrapper}>
            <Text  style={styles.headerText} >Menu</Text>
            </View>
            
          </ScrollView>
        </FadeInView>
      </SafeAreaView>
    );
     }
    if (modalVisible)
    content = (
      <WebViewModal
        modalVisible
        setModalVisible={setModalVisible}
        url={'http://kriolmidia.com/'}
        
      ></WebViewModal>
    );

    else if (!isLoading && (!fetchedData || fetchedData.length === 0)) {
      content = (<View style={styles.containerIsLoading}><Text>Could not fetch any data.</Text></View>);
    }
    return content;

}


const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  containerIsLoading: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },

  headerWrapper:{
    paddingVertical:30,
    paddingHorizontal:10,
    flex:1,
    backgroundColor: "rgba(63, 111, 159, 0.9)",
  },

  headerText:{
    fontSize:30,
    color:"rgba(255, 255, 255, 0.9)",
    fontFamily:"BebasNeue",
    // textAlign:"center",
  },
 


  
});



