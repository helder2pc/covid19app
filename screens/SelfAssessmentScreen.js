import React, { useState } from 'react';
import { View, Text, SafeAreaView, Dimensions, StyleSheet, ActivityIndicator, TouchableOpacity, ScrollView } from 'react-native';
import FadeInView from '../components/FadeInView';
import I18n from '../I18n';
import TitleHeader from '../components/TitleHeader';


const Button = (props) => {
    const { text = '', no, onPress } = props;

    return (
        <TouchableOpacity
            style={no ? styles.answerBtnNo : styles.answerBtn}
            onPress={onPress}
        >
            <Text style={styles.answerText}>{props.text}</Text>
        </TouchableOpacity>
    );
};

export default SelAssessmentScreen = props => {

  const [trigger, setTrigger] = useState(1);


  let content = (


    <SafeAreaView style={styles.mainContainer}>
      <View style={{ height: 100 }}>
        <TitleHeader back title={I18n.t("selfAssessment.title")}  onPress={()=>setTrigger(1)} navigation={props.navigation}/>
      </View>
      <ScrollView
        contentContainerStyle={{ flexGrow: 1, justifyContent: 'center' }}>

        {  
          trigger==1 && 
          <FadeInView>
          <View style={styles.scrollViewContainer}>

            <Text style={styles.questionText} >{I18n.t("selfAssessment.trigger1.question")}</Text>

            <View style={styles.containerAnswer} >
              <Button text={I18n.t("selfAssessment.trigger1.answer1")} onPress={() => setTrigger(2)}></Button>
              <Button text={I18n.t("selfAssessment.trigger1.answer2")} no onPress={() => setTrigger(3)}></Button>
            </View>
          </View>
        </FadeInView>
        }


        {  
          (trigger==2 || trigger==3) && 
          <FadeInView>
          <View style={[styles.scrollViewContainer, {marginTop:0}]}>
            <Text style={styles.questionText} >{I18n.t("selfAssessment.trigger2.question")}</Text>
           

            <View style={styles.containerAnswer} >
              <Button text={I18n.t("selfAssessment.trigger2.answer1")} onPress={() => setTrigger(4)}></Button>
              <Button text={I18n.t("selfAssessment.trigger2.answer2")} no onPress={() => setTrigger(5)}></Button>
            </View>

           { (trigger==2) &&  <Text style={styles.hintText} >{I18n.t("selfAssessment.trigger2.advice")}</Text>}
          </View>
        </FadeInView>
        }   


{  
          (trigger==4) && 
          <FadeInView>
          <View style={styles.scrollViewContainer}>

            <Text style={styles.hintText} >{I18n.t("selfAssessment.trigger4.advice")}</Text>
            
          </View>
        </FadeInView>
        }   


{  
          (trigger==5) && 
          <FadeInView>
          <View style={[styles.scrollViewContainer, {marginTop:0}]}>

            <Text style={styles.questionText} >{I18n.t("selfAssessment.trigger5.question")}</Text>

            <View style={styles.containerAnswer} >
              <Button text={I18n.t("selfAssessment.trigger5.answer3")}  onPress={() => setTrigger(8)}></Button>
              <Button text={I18n.t("selfAssessment.trigger5.answer2")}  onPress={() => setTrigger(7)}></Button>
              <Button text={I18n.t("selfAssessment.trigger5.answer1")} no onPress={() => setTrigger(6)}></Button>

            </View>

          </View>
        </FadeInView>
        }  


    {(trigger==6) && 
          <FadeInView>
          <View style={styles.scrollViewContainer}>

            <Text style={styles.hintText} >{I18n.t("selfAssessment.trigger6.advice")}</Text>

          </View>
        </FadeInView>
        } 
        

        {(trigger==7) && 
          <FadeInView>
          <View style={styles.scrollViewContainer}>

          <Text style={styles.hintText} >{I18n.t("selfAssessment.trigger7.advice")}</Text>


          </View>
        </FadeInView>
        } 

        
    {(trigger==8) && 
          <FadeInView>
          <View style={styles.scrollViewContainer}>

          <Text style={styles.hintText} >{I18n.t("selfAssessment.trigger8.advice")}</Text>


          </View>
        </FadeInView>
        } 






      </ScrollView>


    </SafeAreaView>



  );


  return content;

}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignContent: "center",
  },

  scrollView: {
    height: Dimensions.get('window').height,
  },

  mainContainer: {
    flex: 1
  },

  scrollViewContainer: {
    alignItems: "center",
    paddingHorizontal: 15,
  },

  questionText: {
    fontSize: 28,
    marginBottom: 25,
    color: "#484848",
    textAlign:"center"
  },
  hintText: {
    fontSize: 18,
    marginTop: 25,
    fontWeight: "bold",
    color: "#484848",
    textAlign:"center",
  },
  containerAnswer: {
    // backgroundColor:"red",
    flexDirection: "row",
    flexWrap:"wrap",
    justifyContent:"center"
  },

  answerBtn: {
    display: "flex",
    padding: 10,
    backgroundColor: "rgba(63, 111, 159, 0.7)",
    borderRadius: 8,
    width: "100%",
    marginHorizontal: 30,
    marginBottom: 30,
  },

  answerBtnNo: {
    display: "flex",
    padding: 10,
    backgroundColor: '#C0C0C0',
    borderRadius: 8,
    width: "100%",
    marginHorizontal: 30,
    marginBottom: 30,
  },

  answerText: {
    fontSize: 23,
    color: '#fff',
    textAlign: "center"
  }

});
