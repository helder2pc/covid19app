import React, { useState, useEffect, useRef } from 'react';
import { View, Text, SafeAreaView, ActivityIndicator, StyleSheet, TouchableOpacity, } from 'react-native';
import MapView, { Marker, Circle, Callout, PROVIDER_GOOGLE } from 'react-native-maps';
import mapStyle from './mapStyle.json';
import { MyCluster } from "./MyCluster";
import { useHttp } from './../hooks/http';
import FadeInView from '../components/FadeInView';
import Flag from 'react-native-flags';
import I18n from '../I18n';
import { NavigationEvents } from 'react-navigation';
export default MapScreen = props => {


  let body = "{\"query\":\"query {\\n  allEntries(\\n    where: { latest: true }\\n  ) {\\n    date\\n    country {\\n   code    name\\n      lat\\n      lng\\n    }\\n    newCases\\n    newDeaths\\n    totalCases\\n    totalDeaths\\n    cured\\n    critical\\n  }\\n}\\n\"}";
  const [isLoading, fetchedData] = useHttp(body, [], "allEntries");
  const [mode, setMode] = useState('confirmed');
  const [showMarkers, setShowMarkers] = useState(true);

  let content = (<View style={styles.containerIsLoading}><ActivityIndicator size="large" color="#3f6f9f" /></View>);
  // let content = (<View style={styles.containerIsLoading}><Text>Loading data...</Text></View>);


  if (!isLoading && fetchedData && fetchedData.length > 0) {
    // if (!isLoading) {
    let markersArray = fetchedData.slice();
    content = (
      <SafeAreaView style={{ flex:1}}>

{/* <NavigationEvents
      onWillFocus={payload => console.log('will focus',payload)}
      onDidFocus={payload => console.log('did focus',payload)}
      onWillBlur={payload => console.log('will blur',payload)}
      onDidBlur={payload => console.log('did blur',payload)}
    /> */}
<NavigationEvents
      onWillFocus={() => setShowMarkers(true)}
      onDidBlur={() =>  setShowMarkers(false)}
    />
      <FadeInView>
        <View style={styles.titleContainer}>

          <TouchableOpacity
            onPress={() => { setMode('confirmed') }}
            style={[{ backgroundColor: (mode == 'confirmed') ? 'tomato' : 'transparent', borderColor: (mode == 'confirmed') ? 'tomato' : 'white' },  styles.providerBox]}
          >
            <Text style={styles.providerText}>{I18n.t("simpleText.confirmed")}</Text>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => { setMode('recovered') }}
            style={[{ backgroundColor: (mode == 'recovered') ? '#3CB371' : 'transparent', borderColor: (mode == 'recovered') ? '#3CB371' : 'white', }, styles.providerBox]}
          >
            <Text style={styles.providerText}>{I18n.t("simpleText.recovered")}</Text>
          </TouchableOpacity>

          

          <TouchableOpacity
            onPress={() => { setMode('critical') }}
            style={[{ backgroundColor: (mode == 'critical') ? '#a14a17' : 'transparent', borderColor: (mode == 'critical') ? '#a14a17' : 'white' }, styles.providerBox]}
          >
            <Text style={styles.providerText}>{I18n.t("simpleText.critical")}</Text>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => { setMode('deaths') }}
            style={[{ backgroundColor: (mode == 'deaths') ? '#1f1204' : 'transparent', borderColor: (mode == 'deaths') ? '#1f1204' : 'white'  }, styles.providerBox]}
          >
            <Text style={styles.providerText}>{I18n.t("simpleText.deaths")}</Text>
          </TouchableOpacity>

        </View>
  
        <MapView
          // renderClusterMarker={this.renderCustomClusterMarker}

          provider={PROVIDER_GOOGLE}
          style={styles.map}
          customMapStyle={mapStyle}
          region={{
            latitude: 16.916667,
            longitude: -23.516667,
            latitudeDelta: 150.0922,
            longitudeDelta: 150.0421,
          }}
        >
          {showMarkers && markersArray.map((marker, index) => (
            <Marker
              key={marker.country.code}
              coordinate={{
                latitude: marker.country.lat,
                longitude: marker.country.lng
              }}
            >
              <CustomMarker mode={mode} data={marker}></CustomMarker>

            </Marker>
          ))
          }

        </MapView>

       </FadeInView>
      
    
      </SafeAreaView>
    );
  } 
  // else if (!isLoading && (!fetchedData || fetchedData.length === 0) ) {
  //   content = (<View style={styles.containerIsLoading}><Text>Could not fetch any data.</Text></View>);
  // }
  return content;

}


const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  containerIsLoading: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  map: {
    height: '100%'
  },
  callout: {
    padding: 10,
    width: 180
  },
  title: {
    fontWeight: 'bold',
  },

  titleContainer: {
    flexDirection: "row",
    flexWrap: 'wrap',
    alignItems: "center",
    justifyContent: "center",
    position: "absolute",
    top: 0,
    flex: 1,
    zIndex: 2,
    backgroundColor: "gray",
    paddingHorizontal: 10,
    paddingVertical: 10,

    width: "100%",
    opacity: 0.7,

  },

  providerBox: {
    paddingHorizontal: 10,
    marginHorizontal: 4,
    paddingVertical: 5,
    borderRadius: 4,
    marginBottom: 5,
    borderWidth: 0.5,
    // borderColor: "#d3d3d3",
    flexDirection: "row",
    fontFamily: "BebasNeue",
  },

  providerText: {
    color: "white",
    marginRight: 2,
    fontFamily: "BebasNeue",
  },

  countryFlagContainer:{
      flexDirection:"row",
      alignContent:"center",     
  },
  countryFlagText:{
    marginLeft:6,
    fontSize:18
  }

});


const CustomMarker = props => {
    let count, color;
    if (props.mode == "confirmed") {
        count = props.data.totalCases || 0;
        color = "tomato";
    } else if (props.mode == "recovered") {
        count = props.data.cured || 0;
        color = "#3CB371";
    } else if (props.mode == "critical") {
        count = props.data.critical || 0;
        color = "#a14a17";
    } else if (props.mode == "deaths") {
        count = props.data.totalDeaths || 0;
        color = "#1f1204";
    }

return (

  <>
    <MyCluster color={color} count={count} />

    {/* <MyCluster  color="black" count={props.data.totalDeaths}/> */}
    <Callout >
      <View style={styles.callout}>

      <View style={styles.countryFlagContainer}>
        <Flag code={props.data.country.code} size={24}></Flag>
        <Text style={styles.countryFlagText}>{I18n.t(`countries.${props.data.country.code}`)}</Text>
      </View>


        <Text><Text style={styles.title}>{I18n.t("simpleText.confirmed")}: </Text>{(props.data.totalCases) ? props.data.totalCases : '0'}</Text>
        <Text><Text style={styles.title}>{I18n.t("simpleText.deaths")}: </Text>{(props.data.totalDeaths) ? props.data.totalDeaths : '0'}</Text>
        <Text><Text style={styles.title}>{I18n.t("simpleText.newCase")}: </Text>{(props.data.newCases) ? props.data.newCases : '0'}</Text>
        <Text><Text style={styles.title}>{I18n.t("simpleText.cured")}: </Text>{(props.data.cured) ? props.data.cured : '0'}</Text>
        <Text><Text style={styles.title}>{I18n.t("simpleText.critical")}: </Text>{(props.data.critical) ? props.data.critical : '0'}</Text>


      </View>

    </Callout>
  </>
);
};

const dateFormat = value => {
  if (value < 10)
    return `0${value}`;
  return value;
}

