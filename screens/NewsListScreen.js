
import React, { useState, useEffect } from 'react';
import { View, SafeAreaView, ScrollView, ActivityIndicator, Dimensions, StyleSheet, Text,TouchableOpacity, FlatList } from 'react-native';
import * as rssParser from 'react-native-rss-parser';
import ItemNews from '../components/ItemNews';
import {isValidData} from '../components/FunctionAux';
import I18n from '../I18n';
import WebViewModal from '../components/WebViewModal';
import Flag from 'react-native-flags';
import AsyncStorage from '@react-native-community/async-storage';
import TitleHeader from '../components/TitleHeader';



const NewsListScreen = props => {


  const [loadedChars, setLoadedChars] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [modalVisible, setModalVisible] = useState(false);
  const [url, setUrl] = useState(null);
  const [provider, setProvider] = useState(null);
  const [allArticlesBackup, setAllArticlesBackup] = useState(null);
  const [listProvider, setListProvider] = useState(null);


  useEffect(() => {

    setIsLoading(true);

    // AsyncStorage.removeItem('allNews_time').then()
    AsyncStorage.getItem('allNews_time').then(
      result => {
     
        // alert(result)

        let doResques =false;
        if(result){

          let valid = isValidData(result, 0.5);

          if (valid) {

            AsyncStorage.getItem('allNews').then(
              result => {
            let allArticles = JSON.parse(result);
                // setIsLoading(false);
                if (result && allArticles && allArticles.length > 0) {
                  
                  let providerDist = distinctArray(allArticles);
                  let pv=providerDist.sort((a, b) => (b.language > a.language) ? 1 : (a.language === b.language) ? ((a.source > b.source) ? 1 : -1) : -1 )
                  // alert(pv.length)
                  setListProvider(pv);
                  setAllArticlesBackup(allArticles);
                  const resultFilter = allArticles.filter(item => item.source == pv[0].source);
                  setLoadedChars(resultFilter);
                  setProvider(pv[0]);
                  setIsLoading(false);
                }
              }
            );
            return;
          }
          else{
            doResques=true;
          }
        }
        else{
          doResques=true;
        }
// alert(doResques)
        if(doResques){
         
    fetch("https://covid19-caboverde.herokuapp.com/admin/api", {
      "method": "POST",
      "headers": {
        "content-type": "application/json"
      },
      // "body": "{\"query\":\"query {\\n  allArticles(\\n    where: { status: PUBLISHED },\\n    orderBy: \\\"date_DESC\\\"\\n  ) {   language title date url  source  }\\n}\\n\"}"
      "body": "{\"query\":\"query {\\n  allArticles(\\n    orderBy: \\\"date_DESC\\\"\\n    where: { category: news, status: PUBLISHED }\\n  first: 40 ) {\\n    date\\n    title\\n    language\\n    source\\n    url\\n    imageUrl\\n  }\\n}\\n\"}"
    })
      .then(response => response.json())  // promise
      .then(result => {
       
        if (result && result.data && result.data.allArticles && result.data.allArticles.length > 0) {
         
          let allArticles = result.data.allArticles;
          AsyncStorage.setItem('allNews', JSON.stringify(allArticles)).then( ()=>

         {

            
          let providerDist = distinctArray(allArticles);
          let pv=providerDist.sort((a, b) => (b.language > a.language) ? 1 : (a.language === b.language) ? ((a.source > b.source) ? 1 : -1) : -1 )
          // alert(pv.length)
          setListProvider(pv);
          setAllArticlesBackup(allArticles);
          const resultFilter = allArticles.filter(item => item.source == pv[0].source);
          setLoadedChars(resultFilter);
          setProvider(pv[0]);
          setIsLoading(false);


          let dateNow = new Date();
          AsyncStorage.setItem("allNews_time", dateNow.toString()).then()
         }
          // }
          )

        }

      })
      .catch(err => {
       
        // alert(87)
        // AsyncStorage.getItem('allNews').then(
        //   result => {
        // let allArticles = JSON.parse(result);
        //     // setIsLoading(false);
        //     if (result && allArticles && allArticles.length > 0) {
              
        //       let providerDist = distinctArray(allArticles);
        //       let pv=providerDist.sort((a, b) => (b.language > a.language) ? 1 : (a.language === b.language) ? ((a.source > b.source) ? 1 : -1) : -1 )
        //       // alert(pv.length)
        //       setListProvider(pv);
        //       setAllArticlesBackup(allArticles);
        //       const resultFilter = allArticles.filter(item => item.source == pv[0].source);
        //       setLoadedChars(resultFilter);
        //       setProvider(pv[0]);
        //       setIsLoading(false);
        //     }
        //   }
        // );



      });

    }

    })


  }, []);


  const changeProvider = provider => {
   
   
    setProvider(provider)
    const resultFilter = allArticlesBackup.filter(item => item.source == provider.source);
    setLoadedChars(resultFilter);
   
  }

    let content = (<View style={styles.containerIsLoading}><ActivityIndicator size="large" color="#3f6f9f" /></View>);

    const getHeader = (
     <View >

   <TitleHeader title={I18n.t("title.news")}></TitleHeader>
<View style={styles.listProvider}>
       
 
       <FlatList
                 horizontal={true}
                 data={listProvider}
                 contentContainerStyle={[styles.listProvider]}
                 renderItem={({ item,index }) => <ButtonsItem 
                       data={item} 
                       provider={provider} 
                       changeProvider_={
                            
                               changeProvider
                        


                        }  />}
                       keyExtractor={item => item.title + item.source}
               />

       </View>

      </View>
  );

  if (!isLoading && loadedChars && loadedChars.length > 0) {
   // listProvider.sort((a, b) => (a.country.name > b.country.name) ? 1 : -1)

    content = (
      <SafeAreaView style={{flex:1}} >
                    
           
            {/* <ScrollView   > */}
         
     
  
        
      
     
        {loadedChars && <FlatList 
        ListHeaderComponent={getHeader}
          data={loadedChars}
          renderItem={({ item,index }) => <ItemNews delay={index*50} data={item} setUrl={setUrl} setModalVisible={setModalVisible} />}
          keyExtractor={item => item.url}
        />}

        

{/* </ScrollView> */}

      </SafeAreaView>
    );
  } else if (!isLoading && (!loadedChars || loadedChars.length === 0)) {
     content = (<View style={styles.containerIsLoading}>
       <Text>Could not fetch any data.</Text>
     </View>);
   }

  if (modalVisible)
    content = (
      <WebViewModal
        modalVisible
        setModalVisible={setModalVisible}
        url={url}
        setUrl={setUrl}
      ></WebViewModal>
    );



  return content;
}

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

const styles = StyleSheet.create({
  webview: {
    flex: 1,
    // backgroundColor: 'yellow',
//    width: deviceWidth,
//    height: deviceHeight
  },
  container: {
    flex: 1,
    backgroundColor: '#f5f5f5'
  },
  containerIsLoading: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f5f5f5'
  },
  listProvider: {
    flexDirection: "row",
    marginVertical: 20,
    // flexWrap: "wrap",
    justifyContent: "center",
    alignItems: "center"
  },
  providerBox: {
    justifyContent: "center",
    alignItems: "center",
    paddingHorizontal: 15,
    marginHorizontal: 4,
    paddingVertical: 12,
    borderRadius: 4,
    marginBottom: 5,
    borderWidth: 0.5,
    borderColor: "#d3d3d3",
    flexDirection:"row",
    fontFamily: "BebasNeue",
    
  },
  providerText: {
    color: "white",
    marginRight: 5,
  },


  headerWrapper: {

    paddingVertical: 30,
    paddingHorizontal: 10,
    flex: 1,
    backgroundColor: "rgba(63, 111, 159, 0.9)",
},

headerText: {
    fontSize: 30,
    color: "rgba(255, 255, 255, 0.9)",
    fontFamily: "BebasNeue",
  marginTop:5
    // textAlign:"center",
},

});


const distinctArray = array => {
  const map = [];
  for (let item of array) {
    if (!exist(map, item.source)) {
      map.push(
        item
      );
    }
  }
  return map;
}

const exist = (array, value) => {
  for (let item of array) {
    if (item.source == value) {
      return true;
    }
  }
  return false;
}

export default NewsListScreen;



const ButtonsItem = ({ data,provider, changeProvider_ } ) => {
  
  // alert(data.date)
  return (

  // <Text>TEXT</Text>
  // const  = props;
//  alert(changeProvider)

      <TouchableOpacity key={data.date+data.source} style={[{ backgroundColor: (provider.source == data.source) ? '#194A72' : 'transparent' }, styles.providerBox]} onPress={ ()=>changeProvider_(data)}>
        <Text style={[styles.providerText, { color: (provider.source == data.source) ? 'white' : 'black' }]}>{`${data.source}`}</Text>
        <Flag code={(data.language=='pt')?'PT':'GB'} size={16}
    />
      </TouchableOpacity>

)};
