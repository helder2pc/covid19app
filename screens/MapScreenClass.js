import React, { Component } from 'react';
import { View, Text,Button,StyleSheet } from 'react-native';
import startMainTab from './startMainTab';
import MapView, {Marker, Callout, PROVIDER_GOOGLE } from 'react-native-maps';
import mapStyle from './mapStyle.json'; 
import { MyCluster } from "./MyCluster";
import { ClusterMap } from "react-native-cluster-map";
export default class MapScreen extends Component {

  onButtonPress = () => {
    startMainTab();
  }
  renderCustomClusterMarker = (count) => <MyCluster count={2} />

  showWelcomeMessage = () =>
    Alert.alert(
      'Welcome to San Francisco',
      'The food is amazing',
      [
        {
          text: 'Cancel',
          style: 'cancel'
        },
        {
          text: 'Ok'
        }
      ]
    ) 
  render() {
    return (
 
      <ClusterMap
      renderClusterMarker={this.renderCustomClusterMarker}
          
            provider={PROVIDER_GOOGLE}
            style={styles.map}
            customMapStyle={mapStyle}
            region={{
              latitude: 14.916667,
              longitude: -23.516667, 
              latitudeDelta: 150.0922, 
              longitudeDelta: 150.0421,
            }}
    >
    <Marker coordinate={{ latitude:14.916667, longitude: -23.516667 }} >
    <MyCluster count={78}/>
    <Callout onPress={this.showWelcomeMessage}> 
            <View style={styles.callout}>
               <Text><Text style={styles.title}>Confirmed: </Text>78</Text>
               <Text><Text style={styles.title}>Deaths: </Text>2</Text>
               <Text><Text style={styles.title}>Recoverd: </Text>70</Text>
               
              </View>
            </Callout>
      </Marker>
  {/* <Circle coordinate={{ latitude:15.916667, longitude: -23.516667 }} 
  center={{ latitude:14.916667, longitude: -23.516667 }} 
  radius={1000}
  fillColor="red"
  /> */}
    </ClusterMap>
    ); 
  } 
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  map: {
    height: '100%'
  },
  callout:{
    padding:10,
    width:180
  },
  title:{
      fontWeight:'bold',
  }
  
});