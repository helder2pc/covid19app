import * as React from 'react';
import { Text, View } from 'react-native';
import I18n from './I18n/index'; //'app/i18n/i18n';
import {MapScreen, HomeScreen, MenuScreen,  NewsListScreen, CountryDetailScreen, AboutUsScreen} from './screens/index'; //'app/i18n/i18n';
import Icon from 'react-native-vector-icons/Ionicons'; 
import {createAppContainer} from 'react-navigation';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import {createStackNavigator} from 'react-navigation-stack';
import ArticleDetail from './components/ArticleDetail';
import EmergencyContactsScreen from './screens/EmergencyContactsScreen';
import SelAssessmentScreen from './screens/SelfAssessmentScreen';
  

const HomeStack = createStackNavigator(
  {
    //Defination of Navigaton from home screen
    Feed: { screen: HomeScreen },
    //Feed: { screen: AboutUsScreen },
    //  Feed: { screen: MenuScreen },
    Details: { screen: CountryDetailScreen },
    Articles: { screen: ArticleDetail },
    EmergencyContacts: { screen: EmergencyContactsScreen }, 
    SelAssessment: { screen: SelAssessmentScreen }, 
    About: { screen: AboutUsScreen },

    
  },
  
  {
    defaultNavigationOptions: {
     
      //  header: null,
      headerShown:false,
     // Header customization of the perticular Screen
      // headerStyle: {
      //   backgroundColor: '#42f44b',
      // },
      // headerTintColor: '#FFFFFF',
       title: '',
      //Header title
    },
  }
);

const SettingsStack = createStackNavigator(
  {
    //Defination of Navigaton from setting screen
    Settings: { screen: DicasScreen },
    Details: { screen: NewsListScreen },
    
  },
  {
    defaultNavigationOptions: {
      //Header customization of the perticular Screen
      headerStyle: {
        backgroundColor: '#42f44b',
      },
      headerTintColor: '#FFFFFF',
      title: 'Settings',
      //Header title
    },
  }
);


const App = createBottomTabNavigator(
  {
    //Defination of Navigaton bottom options
    [I18n.t("mainTab.home")]: { screen: HomeStack },
    [I18n.t("mainTab.map")]: { screen: MapScreen },
    [I18n.t("mainTab.help")]: { screen: EmergencyContactsScreen },
    [I18n.t("mainTab.news")]: { screen: NewsListScreen },
    [I18n.t("mainTab.menu")]: { screen: MenuScreen },
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
     
      tabBarIcon: ({ focused, horizontal, tintColor }) => {
        const { routeName } = navigation.state;
        if (routeName == [I18n.t("mainTab.home")]) {
          return (
            <Icon name="ios-home" size={22} color={focused?'#194A72':'gray'} />
            // <Image
            //   source={
            //     focused
            //       ? require('./asset/logo1.png')
            //       : require('./asset/logo2.png')
            //   }
            //   style={{
            //     width: 20,
            //     height: 20,
            //     borderRadius: 40 / 2,
            //   }}
            // />
          );
        } else if (routeName == [I18n.t("mainTab.help")]) {
          return (
            <Icon name="ios-information-circle-outline" size={22} color={focused?'#194A72':'gray'} />
          );
        }

        else if (routeName == [I18n.t("mainTab.map")]) {
          return (
            <Icon name="ios-map" size={22} color={focused?'#194A72':'gray'} />
          );
        }

        else if (routeName == [I18n.t("mainTab.news")]) {
          return (
            <Icon name="ios-globe" size={22} color={focused?'#194A72':'gray'} />
          );
        }

        else if (routeName == [I18n.t("mainTab.menu")]) {
          return (
            <Icon name="ios-menu" size={22} color={'gray'} />
          );
        }
      },
    }),
    tabBarOptions: {
      activeTintColor: '#194A72',
      inactiveTintColor: 'gray',
      transitionConfig: () => ({ screenInterpolator: () => null }),
     
      
      
    },
  }
);


export default createAppContainer(App);

function DicasScreen() {
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Text>{I18n.t('home.introduction')}</Text>
    </View>
  );
}

